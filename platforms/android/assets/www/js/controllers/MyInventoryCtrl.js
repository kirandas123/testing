angular.module('MyInventoryController', ['ionic','ui.router'])
	
.controller('MyInventoryCtrl', function($scope, $state, $cordovaCapture, $ionicModal, $ionicPopup, $cordovaActionSheet, $ionicHistory) {

	var userid = JSON.parse(window.localStorage['loggedUser']).clientid;

	if(window.localStorage['InventoryCategory']){
		$scope.category = window.localStorage['InventoryCategory'];
	}

	$scope.getPolicies = function() {
		$scope.apiCall('GET', '/policies/' + userid, {}, true);
	};

	$scope.savePolicy = function(policy) {
		window.localStorage['InventoryPolicy'] = JSON.stringify(policy);
		$state.go('app.myinventoryitems', {});
	};

	$scope.getCategory = function(choosenCategory){
		window.localStorage['InventoryCategory'] = choosenCategory;
	};

	$scope.getInventory = function(){
		var policynumber = JSON.parse(window.localStorage['InventoryPolicy']).policynumber;
		$scope.apiCall('POST', '/myinventory', {policynumber: policynumber}, true);	
	};

	$scope.addItemDetails = function(){
		$state.go('app.myinventoryadd', {
			inventoryCategory: $scope.category
		});
	};

	$scope.openItemDetails = function (id){
		$state.go('app.myinventorydetails', {
			inventoryId: id
		});
	};
	
	$scope.alertAutoDismissed = function() {
		ActivityIndicator.hide();
	};

	/* ***************************************** */
	/*                 API Calls                 */
	/* ***************************************** */
	$scope.apiCall = function(methodType, resource, data, isAsync) { 
		// var apiURL = 'http://127.0.0.1:3000/api';
		// var apiURL = 'http://192.168.10.104:3000/api';
		var apiURL = 'http://dashboard.clientdesk.co:3000/api';
		var apiResource = resource;
		var apiURI  = apiURL + apiResource;

		var credentials = {
			id: 'core',
			key: 'password',
			algorithm: 'sha256'
		}

		var requestOptions = {
			uri: apiURI,
			method: methodType,
			headers: {}
		};

		var header = hawk.client.header(apiURI, methodType, { credentials: credentials, ext: 'some-app-data' });
		requestOptions.headers.Authorization = header.field;

		$.ajaxSetup({
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
				xhr.setRequestHeader('Authorization', requestOptions.headers.Authorization);
			}
		});

		// Request with custom header
		$.ajax({
			async: isAsync,
			type: methodType,
			url: apiURI,
			success: function(response){

				if(methodType === "GET"){
					var aPolicyArray = [];
					var pPolicyArray = [];

					for (var i = 0; i < response.length; i++) {
						if (response[i].status == "Active") {
							if (response[i].type == "Automotive") {
								aPolicyArray.push(response[i]);
							}
							if (response[i].type == "Property") {
								pPolicyArray.push(response[i]);
							}
						}
					};

					$scope.aPolicies = aPolicyArray;
					$scope.pPolicies = pPolicyArray;
				}

				if (methodType == "POST"){

					var savedInventories = [], itemsCount = 0, TotalPrice = 0, coverageAmount = 500000;

					for (var i = 0; i < response.length; i++){
						itemsCount++;
						TotalPrice = parseFloat(TotalPrice) + parseFloat(response[i].price);
						savedInventories.push(response[i]);
					}

					if (itemsCount == 0){
						var ItemObject = {
							count : 0,
							total: 0,
							coverageAmount: coverageAmount,
							balanceStatus: "No Items to show",
							className: "dark"
						};
					} else if (TotalPrice > coverageAmount){
						var overLimit = TotalPrice - coverageAmount;
						var ItemObject = {
							count : itemsCount,
							total: TotalPrice,
							coverageAmount: coverageAmount,
							balanceStatus: "Your total overlimit by $" + overLimit + "",
							className: "assertive"
						};
					} else if (TotalPrice == coverageAmount){
						var ItemObject = {
							count : itemsCount,
							total: TotalPrice,
							coverageAmount: coverageAmount,
							balanceStatus: "You reached the coverage limit.",
							className: "royal"
						};
					} else {
						var inventoryAmountBalance = coverageAmount - TotalPrice;
						var ItemObject = {
							count : itemsCount,
							total: TotalPrice,
							coverageAmount: coverageAmount,
							balanceStatus: "Balance remaining: $" + inventoryAmountBalance + "",
							className: "calm"
						};
					}

					$scope.itemObj = ItemObject;
					$scope.items = savedInventories;
				}

				$scope.$apply();
			},
			error: function(response) {
				// alert(JSON.stringify(response));
				navigator.notification.alert("Sorry, an error has occurred", $scope.alertAutoDismissed(), "Error", "OK");
			},
			data: JSON.stringify(data),
			dataType: 'json'
		});
	};
});