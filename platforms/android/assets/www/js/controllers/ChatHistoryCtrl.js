angular.module('ChatHistoryController', ['ionic','ui.router'])

.controller('ChatHistoryCtrl', function($scope, ChatHistory, Requests, $state, $stateParams, $timeout, $window) {

	var sessionDetails;
	console.log("selected room" + $state.params.sessionKey);
	console.log("selected room" + $state.params.sessionStatus);
	if ($state.params.sessionStatus == "closed" || $state.params.sessionStatus == "closed by client") {
		var messageTextInput=document.getElementById("messageTextInput");
		messageTextInput.parentNode.removeChild(messageTextInput);
		var sendButton=document.getElementById("sendButton");
		sendButton.parentNode.removeChild(sendButton);

		var messageTextInputDiv=document.getElementById("messageTextInputDiv");
		sessionStatus = "Session is closed";
		var statusText = document.createTextNode(sessionStatus);
		messageTextInputDiv.appendChild(statusText);
	}

	else if ($state.params.sessionStatus == "temporarily closed") {
		var reconnectButton=document.getElementById("reconnectButton");
		reconnectButton.disabled = false;

		var messageTextInput=document.getElementById("messageTextInput");
		messageTextInput.parentNode.removeChild(messageTextInput);
		var sendButton=document.getElementById("sendButton");
		sendButton.parentNode.removeChild(sendButton);

		var messageTextInputDiv=document.getElementById("messageTextInputDiv");
		sessionStatus = "Session is temporarily closed. Press Reconnect to get back to the session";
		var statusText = document.createTextNode(sessionStatus);
		messageTextInputDiv.appendChild(statusText);
	}

	getChats();
	
	function getChats(){

		$scope.IM = {
			textMessage: ""
		};

		ChatHistory.setSession($state.params.sessionKey);

		setTimeout(function () {

			var chatHistory = ChatHistory.allChats();
			if (chatHistory) {
				console.log("His: " + chatHistory);
				$scope.roomName = " - " + $state.params.sessionKey;
				console.log($scope.roomName);
				$scope.chats = chatHistory;
				$scope.$apply();
			}
		},1000);
		//var chatHistory = ChatHistory.allChats();
	};

	$scope.sendMessage = function (msg) {
		console.log(msg);
		//var authData = JSON.parse(window.localStorage['authData']);
		//console.log(authData);
		var user = {
			//displayName : authData.password.email
			displayName : "admin@clientdesk.co"
		}
		//console.log(firebase);
		ChatHistory.send(user, msg);
		$scope.IM.textMessage = "";
	};

	// $scope.remove = function (chat) {
	//     ChatHistory.remove(chat);
	// }

	$scope.quitSession = function () {
		if ($state.params.sessionStatus == "closed" || $state.params.sessionStatus == "closed by client") {
			$window.history.back();
		}
		else{
			var ref = new Firebase("https://clientdesk.firebaseio.com/sessions/" + $state.params.sessionKey + "/");
			ref.update({
				status: "closed by client"
			});

			$window.history.back();
		}
	};

	$scope.reconnectSession = function () {
		var reqId = Requests.sendReconnectRequest("0", "Client1", $state.params.department, $state.params.sessionKey);
		
		$state.go('chat', {
			requestID: reqId,
			department: $state.params.department,
			clientname: 'Client1',
			clientid: '0'
		});
	};
});