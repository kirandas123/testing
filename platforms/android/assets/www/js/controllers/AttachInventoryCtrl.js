angular.module('AttachInventoryController', ['ionic', 'ui.router'])

.controller('AttachInventoryCtrl', function($scope, $state, $cordovaCapture, $ionicModal, $ionicPopup, $cordovaActionSheet, $ionicHistory) {

	var policynumber = JSON.parse(window.localStorage['selectedPolicy']).policynumber;

	var savedInventories = [], selectedItems = [], storedIds = [];
	var selectedCategory = window.localStorage['claimType'];

	if (selectedCategory == "auto") $scope.category = "Automotive";
	if (selectedCategory == "property") $scope.category = "Property";

	$scope.itemsSelected = selectedItems;
	$scope.totalPrice = 0;
	$scope.items = [];

	$scope.saveItems = function(Id){
		
		var found = false;
		var index = null;
		var item = null;
		$scope.totalPrice = 0;
		
		for (var i = 0; i < savedInventories.length; i++){
			if (savedInventories[i]._id == Id){
				item = savedInventories[i];
			}
		}
		
		for (var i = 0; i < selectedItems.length; i++) {
			if (selectedItems){
				if (selectedItems[i]._id == Id){
					found = true;
					index = i;
				}
			}
		}

		if (found == true){
			selectedItems.splice(index, 1);
			var itemClicked = document.getElementById(Id);
			itemClicked.className = "inventoryCapImg";
		}
		else{
			var itemClicked = document.getElementById(Id);
			itemClicked.className += " " + "selectedItem";
			selectedItems.push(item);
		}

		for (i = 0; i < selectedItems.length; i++){
			$scope.totalPrice = parseFloat($scope.totalPrice) + parseFloat(selectedItems[i].price);
		}

		$scope.itemsSelected = selectedItems;
	};
	
	$scope.goBack = function () {
		$ionicHistory.goBack();
	};

	$scope.goNext = function () {

		if (selectedItems.length > 0){
			window.localStorage.removeItem('inventoryItems');
			window.localStorage['inventoryItems'] = JSON.stringify(selectedItems);

			if ($scope.category == "Automotive"){
				$state.go('app.summary', {cache: false});
			}
			if ($scope.category == "Property"){
				$state.go('app.affected', {cache: false});
			}
		}
		else{
			navigator.notification.alert("Please, choose at least one item.", $scope.alertAutoDismissed(), "Validation", "OK");
		}
	};

	$scope.skipStep = function(){
		
		window.localStorage['inventoryItems'] = JSON.stringify(selectedItems);

		if ($scope.category == "Automotive"){
			$state.go('app.summary', {cache: false});
		}
		if ($scope.category == "Property"){
			$state.go('app.affected', {cache: false});
		}
	};

	$scope.alertAutoDismissed = function() {
		ActivityIndicator.hide();
	};

	/* ***************************************** */
	/*                 API Calls                 */
	/* ***************************************** */
	$scope.apiCallForInventories = function (methodType, resource, data, isAsync) { 
		
		// var apiURL = 'http://127.0.0.1:3000/api';
		// var apiURL = 'http://192.168.10.104:3000/api';
		var apiURL = 'http://dashboard.clientdesk.co:3000/api';
		var apiResource = resource;
		var apiURI  = apiURL + apiResource;

		var credentials = {
			id: 'core',
			key: 'password',
			algorithm: 'sha256'
		}

		var requestOptions = {
			uri: apiURI,
			method: methodType,
			headers: {}
		};

		var header = hawk.client.header(apiURI, methodType, { credentials: credentials, ext: 'some-app-data' });
		requestOptions.headers.Authorization = header.field;

		var responseJSON = null;

		$.ajaxSetup({
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
				xhr.setRequestHeader('Authorization', requestOptions.headers.Authorization);
			}
		});

		// Request with custom header
		$.ajax({
			async: isAsync,
			type: methodType,
			url: apiURI,
			success: function(response){
				for (var i=0; i<response.length; i++){
					if (response[i].category == $scope.category){
						if (response[i].status == 'Approved'){
							delete response[i].policynumber;
							savedInventories.push(response[i]);
						}
					}//end of if
				} //end of for loop

				$scope.items = savedInventories;
			},
			error: function(response) {
				// alert(JSON.stringify(response));
				navigator.notification.alert("Sorry, an error has occurred", $scope.alertAutoDismissed(), "Error", "OK");
			},
			data: JSON.stringify(data),
			dataType: 'json'
		});
	};

	$scope.apiCallForInventories('POST', '/myinventory', {policynumber: policynumber} ,true);
});