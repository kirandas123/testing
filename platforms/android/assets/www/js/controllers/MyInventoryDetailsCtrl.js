angular.module('MyInventoryDetailsController', ['ionic','ui.router'])
	
.controller('MyInventoryDetailsCtrl', function($scope, $state, $cordovaCapture, $ionicModal, $ionicPopup, $cordovaActionSheet, $ionicHistory, $ionicLoading) {

	var APIResponse;
	
	function deleteInventory(){
		$scope.apiCallForInventory('DELETE', '/inventory/' + $state.params.inventoryId ,{},true);
		$state.go('app.myinventory', {});
		// $scope.goBack();
	}

	$scope.confirmDeleteInventory = function(){
		var confirmPopup = $ionicPopup.confirm({
			title: 'Delete',
			template: 'Are you sure you want to delete the item?'
		});
		confirmPopup.then(function(res) {
			if(res) {
				deleteInventory();
			} else {
				console.log('cancel delete');
			}
		});
	};

	function setInventoryDetails(){
		var Inventory = {
			name: APIResponse.name,
			brand: APIResponse.brand,
			price: APIResponse.price,
			date: APIResponse.dateofpurchase,
			model: APIResponse.modelno,
			approvalStatus: APIResponse.status, 
			images: APIResponse.imagedetails
		};
	
		// for(var i = 0; i < APIResponse.imagedetails.length; i++){
		// 	var url = APIResponse.imagedetails[i].url;
		// 	var imageRow = document.getElementById('imageRow');
		// 	var imageColumn = document.createElement("div");
		// 	imageColumn.className = "col col-33";
		// 	var imageTag = document.createElement("img");
		// 	imageTag.className = "col ";
		// 	imageTag.src = url;
		// 	imageColumn.appendChild(imageTag);
		// 	imageRow.appendChild(imageColumn);
		// }
		$scope.Inventory = Inventory;
	};

	$scope.alertAutoDismissed = function() {
		ActivityIndicator.hide();
	};

	$scope.apiCallForInventory = function (methodType, resource, data, isAsync) { 
		
		// var apiURL = 'http://127.0.0.1:3000/api';
		// var apiURL = 'http://192.168.10.104:3000/api';
		var apiURL = 'http://dashboard.clientdesk.co:3000/api';
		
		var apiResource = resource;
		var apiURI  = apiURL + apiResource;

		var credentials = {
			id: 'core',
			key: 'password',
			algorithm: 'sha256'
		}

		var requestOptions = {
			uri: apiURI,
			method: methodType,
			headers: {}
		};

		var header = hawk.client.header(apiURI, methodType, { credentials: credentials, ext: 'some-app-data' });
		requestOptions.headers.Authorization = header.field;
		var responseJSON = null;

		$.ajaxSetup({
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
				xhr.setRequestHeader('Authorization', requestOptions.headers.Authorization);
			}
		});

		// Request with custom header
		$.ajax({
			async: isAsync,
			type: methodType,
			url: apiURI,
			success: function(response){
				savedInventories = response;
				//alert(JSON.stringify(response));
				APIResponse = response;
				console.log(APIResponse);
				if(methodType == "GET"){
					setInventoryDetails();
				}
				if(methodType == "DELETE"){
					var alertPopup = $ionicPopup.alert({
						title: 'Success',
						template: 'Item deleted successfully'
					});
					alertPopup.then(function(res) {
						$scope.goBack();
					});
				}
			},
			error: function(response) {
				// alert(JSON.stringify(response));
				navigator.notification.alert("Sorry, an error has occurred", $scope.alertAutoDismissed(), "Error", "OK");
			},
			data: JSON.stringify(data),
			dataType: 'json'
		});
	};

	$scope.apiCallForInventory('GET', '/inventory/' + $state.params.inventoryId ,{},true);
});