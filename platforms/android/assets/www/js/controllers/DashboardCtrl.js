angular.module('DashboardController', ['ionic','ui.router'])

.controller('DashboardCtrl', function($scope, $ionicModal, $state, $stateParams, $cordovaSQLite, $cordovaInAppBrowser, $ionicPopup, $http,  $ionicHistory,  $ionicLoading) {
	
	$scope.doLogout = function() {
		window.localStorage.clear();
		$state.go('app.login', {});
	};

	$scope.alertAutoDismissed = function() {
		ActivityIndicator.hide();
	};
	
	$scope.fbLogin = function() {
		var fbLoginSuccess = function (userData) {
			navigator.notification.alert(userData, $scope.alertAutoDismissed(), "UserInfo", "OK");
		};
		
		facebookConnectPlugin.login(["public_profile"], fbLoginSuccess, function (error) {
			navigator.notification.alert("Error: " + error, $scope.alertAutoDismissed(), "Login Error", "OK");
		});
		// facebookConnectPlugin.login(["user_likes"], fbLoginSuccess, function (error) {
		// 	navigator.notification.alert("Error: " + error, $scope.alertAutoDismissed(), "Login Error", "OK");
		// });
		// facebookConnectPlugin.login(["user_events"], fbLoginSuccess, function (error) {
		// 	navigator.notification.alert("Error: " + error, $scope.alertAutoDismissed(), "Login Error", "OK");
		// });
	};
	
	$scope.saveProfile = function () {

		var policy = "";
		var policyholder = "";
		var company = "";

		policy = document.querySelector("#policy").value;
		policyholder = document.querySelector("#policyholder").value;
		company = document.querySelector("#company").value;

		service = document.querySelector("#serviceNumber").value;
		alert = document.querySelector("#alertNumber").value;
		emergency = document.querySelector("#emergencyNumber").value;

		var oProfile = {
			policy : policy,
			policyholder : policyholder,
			company : company,
			service : service,
			alert : alert,
			emergency : emergency
		};

		//save to local storage
		window.localStorage['profile'] = JSON.stringify(oProfile);

		$scope.apiCall('POST', '/profile', oProfile);
		
		$ionicHistory.nextViewOptions({
			disableAnimate: false,
			disableBack: true
		});
		
		$ionicHistory.clearHistory();
		$state.go('app.dashboard', {});
	};

	$scope.goBack = function () {
		$ionicHistory.goBack();
	};

	ionic.Platform.ready(function() {

		var scheduledPrompts;
		$scope.apiCallForPrompts = function (methodType, resource, data, isAsync) { 

			// var apiURL = 'http://127.0.0.1:3000/api';
			// var apiURL = 'http://192.168.10.104:3000/api';
			var apiURL = 'http://dashboard.clientdesk.co:3000/api';
			var apiResource = resource;
			var apiURI  = apiURL + apiResource;

			var credentials = {
				id: 'core',
				key: 'password',
				algorithm: 'sha256'
			}

			var requestOptions = {
				uri: apiURI,
				method: methodType,
				headers: {}
			};

			var header = hawk.client.header(apiURI, methodType, { credentials: credentials, ext: 'some-app-data' });
			requestOptions.headers.Authorization = header.field;

			var responseJSON = null;

			$.ajaxSetup({
				beforeSend: function(xhr) {
					xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
					xhr.setRequestHeader('Authorization', requestOptions.headers.Authorization);
				}
			});

			// Request with custom header
			$.ajax({
				async: isAsync,
				type: methodType,
				url: apiURI,
				success: function(response){
					scheduledPrompts = response;
				},
				error: function(response) {
					//responseJSON = JSON.stringify(response);
					//alert(JSON.stringify(response));
				},
				data: JSON.stringify(data),
				dataType: 'json'
			});
			// scheduledPrompts = responseJSON;
		}

		// console.log($state.current.name);
		var delay=5000;//10 seconds
		$scope.apiCallForPrompts('GET', '/scheduledprompt',{},true);

		function dateCheck(from,to,check) {

			var fDate,lDate,cDate;
			fDate = Date.parse(from);
			lDate = Date.parse(to);
			cDate = Date.parse(check);

			if((cDate <= lDate && cDate >= fDate)) {
				return true;
			}
			return false;
		}

		function storeInformationMails(mailId, type, title, description, showndate) {
			console.log(showndate);
			var query = "INSERT INTO mails (mailId, type, title, description, showndate) VALUES (?,?,?,?,?)";
			$cordovaSQLite.execute(db, query, [mailId, type, title, description, showndate]).then(function(res) {
				console.log("INSERT ID -> " + res.insertId);
			}, function (err) {
				console.error(err);
			});
		}

		function storeActionMails(mailId, type, title, description, url, showndate) {
			console.log(showndate);
			var query = "INSERT INTO mails (mailId, type, title, description, url, showndate) VALUES (?,?,?,?,?,?)";
			$cordovaSQLite.execute(db, query, [mailId, type, title, description, url, showndate]).then(function(res) {
				console.log("INSERT ID -> " + res.insertId);
			}, function (err) {
				console.error(err);
			});
		}

		function storeQuestionMails(mailId, type, title, description, url, message, scenario, showndate) {
			console.log(showndate);
			var query = "INSERT INTO mails (mailId, type, title, description, url, message, scenario, showndate) VALUES (?,?,?,?,?,?,?,?)";
			$cordovaSQLite.execute(db, query, [mailId, type, title, description, url, message, scenario, showndate]).then(function(res) {
				console.log("INSERT ID -> " + res.insertId);
			}, function (err) {
				console.error(err);
			});
		}

		var options = {
			location: 'yes',
			clearcache: 'yes',
			toolbar: 'yes',
			toolbarposition: 'top',
			closebuttoncaption: 'Back to App'
		};

		$scope.$on('$cordovaInAppBrowser:exit', function(e, event){
			promptsCounter++;
			showPromptsPopUp();
		});

		// localStorage.clear();
		var scheduledPromptsToShow = [];
		var promptsCounter = 0;

		function showPromptsPopUp(){

			if(scheduledPromptsToShow[promptsCounter]){
				if(scheduledPromptsToShow[promptsCounter].prompttype == "action"){
					storeActionMails(scheduledPromptsToShow[promptsCounter]._id, scheduledPromptsToShow[promptsCounter].prompttype, scheduledPromptsToShow[promptsCounter].title, scheduledPromptsToShow[promptsCounter].description, scheduledPromptsToShow[promptsCounter].url, new Date());
					var confirmPopup = $ionicPopup.confirm({
						title: scheduledPromptsToShow[promptsCounter].title,
						template: scheduledPromptsToShow[promptsCounter].description,
						buttons: [{
							text: 'Dismiss',
							type: 'button-default',
							onTap: function(e) {
								console.log("Dismiss");
								promptsCounter++;
								showPromptsPopUp();
							}
						}, {
							text: 'Learn More',
							type: 'button-assertive',
							onTap: function(e) {
								// console.log("The URL = " + scheduledPromptsToShow[promptsCounter].url);
								$cordovaInAppBrowser.open("http://" + scheduledPromptsToShow[promptsCounter].url, '_blank', options)
									.then(function(event) {
										// success
									})
									.catch(function(event) {
										// error
									});
								// promptsCounter++;
								// showPromptsPopUp();
							}   
						}]
					});
				}
				else if(scheduledPromptsToShow[promptsCounter].prompttype == "info"){
					//info popup
					storeInformationMails(scheduledPromptsToShow[promptsCounter]._id, scheduledPromptsToShow[promptsCounter].prompttype, scheduledPromptsToShow[promptsCounter].title, scheduledPromptsToShow[promptsCounter].description, new Date());

					var alertPopup = $ionicPopup.alert({
						title: scheduledPromptsToShow[promptsCounter].title,
						template: scheduledPromptsToShow[promptsCounter].description
					});
					alertPopup.then(function(res) {
						promptsCounter++;
						showPromptsPopUp();
					});
				}
				else{
					storeQuestionMails(scheduledPromptsToShow[promptsCounter]._id, scheduledPromptsToShow[promptsCounter].prompttype, scheduledPromptsToShow[promptsCounter].title, scheduledPromptsToShow[promptsCounter].description, scheduledPromptsToShow[promptsCounter].url, scheduledPromptsToShow[promptsCounter].message, scheduledPromptsToShow[promptsCounter].scenario, new Date());

					if(scheduledPromptsToShow[promptsCounter].scenario == "scenario2"){
						//scenario1
						var confirmPopup = $ionicPopup.confirm({
								title: scheduledPromptsToShow[promptsCounter].title,
								template: scheduledPromptsToShow[promptsCounter].description,
								buttons: [{
									text: 'Yes',
									type: 'button-default',
									onTap: function(e) {
										console.log("Yes");
										var alertPopup = $ionicPopup.alert({
											title: "Thank You",
											template: scheduledPromptsToShow[promptsCounter].message
										});
										alertPopup.then(function(res) {
											promptsCounter++;
											showPromptsPopUp();
										});
									}
								}, {
									text: 'No',
									type: 'button-assertive',
									onTap: function(e) {
										console.log("The URL = " + scheduledPromptsToShow[promptsCounter].url);
										$cordovaInAppBrowser.open("http://" + scheduledPromptsToShow[promptsCounter].url, '_blank', options)
											.then(function(event) {
												// success
											})
											.catch(function(event) {
												// error
											});
											// promptsCounter++;
											//showPromptsPopUp();
									}
								}]
						});
					}
					else{
						//scenario2
						var confirmPopup = $ionicPopup.confirm({
							title: scheduledPromptsToShow[promptsCounter].title,
							template: scheduledPromptsToShow[promptsCounter].description,
							buttons: [{
								text: 'Yes',
								type: 'button-assertive',
								onTap: function(e) {
									console.log("The URL = " + scheduledPromptsToShow[promptsCounter].url);
									$cordovaInAppBrowser.open("http://" + scheduledPromptsToShow[promptsCounter].url, '_blank', options)
										.then(function(event) {
										// success
										})
										.catch(function(event) {
										// error
										});
									// promptsCounter++;
									//showPromptsPopUp();
								}
							},{
								text: 'No',
								type: 'button-default',
								onTap: function(e) {
									console.log("No");
									var alertPopup = $ionicPopup.alert({
										title: "Thank You",
										template: scheduledPromptsToShow[promptsCounter].message
									});
									alertPopup.then(function(res) {
										promptsCounter++;
										showPromptsPopUp();
									});
								}
							}]
						});
					}
				}
			}
		}

		setTimeout(function(){
			// console.log("Prompts " + JSON.stringify(scheduledPrompts));
			if(scheduledPrompts){
				var currentDate = new Date();
				currentDate.setHours(0,0,0,0);

				var shownPrompts = JSON.parse(window.localStorage.getItem("shownPrompts"));
				// console.log("Shown Prompts: " + JSON.stringify(shownPrompts));

				var saveShownPrompts =[];
				
				if (shownPrompts) {
					saveShownPrompts = shownPrompts;
					// console.log("Loaded from memory: " + JSON.stringify(saveShownPrompts));
				}

				// loop API prompts
				for(var i=0; i<scheduledPrompts.length; i++){
					var isAlreadyShownToday = false;

					// checking if already displayed
					for(var j=0; j<saveShownPrompts.length; j++){
				
						// var savedID = saveShownPrompts[j].promptId;
						if(saveShownPrompts[j].promptId == scheduledPrompts[i]._id){
							if(dateCheck(saveShownPrompts[j].shownDate,saveShownPrompts[j].shownDate,currentDate)){
								isAlreadyShownToday = true;
								break;
							}
						}
					}

					if(!isAlreadyShownToday){
						var startDate = new Date(scheduledPrompts[i].startdate);
						startDate.setHours(0,0,0,0);
						var endDate = new Date(scheduledPrompts[i].enddate);
						endDate.setHours(0,0,0,0);
						// console.log("Start Date " + startDate);
						// console.log("End Date " + endDate);
						// console.log("Current Date " + currentDate);

						if(dateCheck(startDate,endDate,currentDate)){
							saveShownPrompts.push({ promptId: scheduledPrompts[i]._id, shownDate: currentDate });
							window.localStorage.setItem("shownPrompts", JSON.stringify(saveShownPrompts));
							// console.log("Popped Up : " + JSON.stringify(saveShownPrompts));
							scheduledPromptsToShow.push(scheduledPrompts[i]);
						} // end of if(dateCheck(startDate,endDate,currentDate))
					} // end of if(!isAlreadyShownToday)
				} // end of for loop
			} // end scheduled prompts

			// console.log("Scheduled prompts to show: " + scheduledPromptsToShow);
			showPromptsPopUp();
		},delay);
	}); // end of ionic platform

	/* ***************************************** */
	/*                 API Calls                 */
	/* ***************************************** */
	$scope.apiCall = function (methodType, resource, data) { 
	
		var apiURL = 'http://127.0.0.1:3000/api';
		// var apiURL = 'http://192.168.10.104:3000/api';
		// var apiURL = 'http://dashboard.clientdesk.co:3000/api';
		var apiResource = resource;
		var apiURI  = apiURL + apiResource;

		var credentials = {
			id: 'core',
			key: 'password',
			algorithm: 'sha256'
		}

		var requestOptions = {
			uri: apiURI,
			method: methodType,
			headers: {}
		};

		var header = hawk.client.header(apiURI, methodType, { credentials: credentials, ext: 'some-app-data' });

		requestOptions.headers.Authorization = header.field;

		var xmlhttp;

		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange=function(){
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				console.log(xmlhttp.responseText);
			}
			if (xmlhttp.readyState==4 && xmlhttp.status!=200) {
				console.log(xmlhttp.responseText);
			}
		}

		// Send authenticated request
		var isAuthorized = hawk.client.authenticate(xmlhttp, credentials, header.artifacts, null);

		var payload = JSON.stringify(data);

		if (isAuthorized) {
			xmlhttp.open(methodType, apiURI, true);
			xmlhttp.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
			xmlhttp.setRequestHeader('Authorization', requestOptions.headers.Authorization);
			//xmlhttp.setRequestHeader("Content-length", payload.length);
			//xmlhttp.setRequestHeader("Connection", "close");
			xmlhttp.send(payload);
		}
	};

	$scope.serviceCall = function() {
		
		var oProfile = window.localStorage['profile'];

		if (oProfile) {
			var number = oProfile.service;
			$scope.callToNumber(number);
		}
	};

	$scope.alertCall = function() {

		var oProfile = window.localStorage['profile'];

		if (oProfile) {
			var number = oProfile.alert;
			$scope.callToNumber(number);
		}
	};

	$scope.emergencyCall = function() {

		var oProfile = window.localStorage['profile'];

		if (oProfile){
			var number = oProfile.emergency;
			$scope.callToNumber(number);
		}
	};

	$scope.callToNumber = function(number){
		
		window.plugins.CallNumber.callNumber(function success() {
			console.log("success");
		}, function error(error) {
			console.log(error);
		}, number);
	};

	$scope.showHideMenu = function() {

		var oList1 = document.getElementById("item1");

		if (oList1.className == "item1" ) {
			oList1.className = "item1 launch";
			document.getElementById("item2").className = "item2 launch";
			document.getElementById("item3").className = "item3 launch";
			document.getElementById("item4").className = "item4 launch";
			document.getElementById("item5").className = "item5 launch";
			document.getElementById("activeMenuBG").className = "activeMenuBGon";
		}
		else {
			oList1.className = "item1";
			document.getElementById("item2").className = "item2";
			document.getElementById("item3").className = "item3";
			document.getElementById("item4").className = "item4";
			document.getElementById("item5").className = "item5";
			document.getElementById("activeMenuBG").className = "activeMenuBGoff";
		}
	};

	$scope.getPolicy = function() {
		$scope.apiCall('GET', '/policies/'+$scope.client.clientid, {}, true);
	};

	$scope.getClient = function() {
		$scope.client = JSON.parse(window.localStorage['loggedUser']);
		$scope.getPolicy();
	};

	/* ***************************************** */
	/*                 API Calls                 */
	/* ***************************************** */
	$scope.apiCall = function (methodType, resource, data, isAsync) { 
		// var apiURL = 'http://127.0.0.1:3000/api';
		// var apiURL = 'http://192.168.10.104:3000/api';
		var apiURL = 'http://dashboard.clientdesk.co:3000/api';
		var apiResource = resource;
		var apiURI  = apiURL + apiResource;

		var credentials = {
			id: 'core',
			key: 'password',
			algorithm: 'sha256'
		}

		var requestOptions = {
			uri: apiURI,
			method: methodType,
			headers: {}
		};

		var header = hawk.client.header(apiURI, methodType, { credentials: credentials, ext: 'some-app-data' });
		
		requestOptions.headers.Authorization = header.field;

		var responseJSON = null;

		$.ajaxSetup({
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
				xhr.setRequestHeader('Authorization', requestOptions.headers.Authorization);
			}
		});

		// Request with custom header
		$.ajax({
			async: isAsync,
			type: methodType,
			url: apiURI,
			success: function(response){
				$scope.policy = response[0];
				$scope.policy.expirydate = new Date($scope.policy.expirydate);
			},
			error: function(response) {
				console.log(responseJSON);
			},
			data: JSON.stringify(data),
			dataType: 'json'
		});
	};

	$scope.resetStorage = function(){
		
		var loggedUser = window.localStorage['loggedUser'];
		window.localStorage.clear();
		window.localStorage['loggedUser'] = loggedUser;
		
		$scope.getClient();
	};
});