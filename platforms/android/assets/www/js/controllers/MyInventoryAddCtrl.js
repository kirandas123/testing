angular.module('MyInventoryAddController', ['ionic','ui.router'])

.controller('MyInventoryAddCtrl', function($scope, $state, $cordovaCapture, $ionicModal, $ionicPopup, $cordovaActionSheet, $ionicHistory, $ionicLoading) {

	var userid = JSON.parse(window.localStorage['loggedUser']).clientid;
	var policynumber = JSON.parse(window.localStorage['InventoryPolicy']).policynumber;

	$ionicModal.fromTemplateUrl('templates/mediacaptuer-choose.html', {
		id: '1',
		scope: $scope,
		animation: 'fade',
		backdropClickToClose: true
	}).then(function(modal) {
		$scope.oModal1 = modal;
	});

	// Open + Close Modals
	$scope.openModal = function(index) {
		if(index == 1)
			$scope.oModal1.show();
	};

	$scope.closeModal = function(index) {
		if(index == 1)
		$scope.oModal1.hide();
	};

	/* Listen for broadcasted messages */
	$scope.$on('modal.shown', function(event, modal) {
		console.log('Modal ' + modal.id + ' is shown!');

		$scope.$on('modal.hidden', function(event, modal) {
			console.log('Modal ' + modal.id + ' is hidden!');
		});

		// Cleanup the modals when we're done with them (i.e: state change)
		// Angular will broadcast a $destroy event just before tearing down a scope 
		// and removing the scope from its parent.
		$scope.$on('$destroy', function() {
			console.log('Destroying modals...');
			$scope.oModal1.remove();
		});
	});

	var clickedButtonId;
	var itemImage1,itemImage2,receiptImage;
	var itemName, itemBrand, itemPrice, itemDate, itemModelNo;
	var itemObject;
	var image1Response, image2Response, image3Response;
	var imageDetailsArray = [], submitedImagesArray = [];

	$scope.submitImageFiles = function (){

		var CONFIG = {
			application_id: 20488,
			auth_key: 'uv8BZKAkhmg7966',
			authSecret: 'NzDqwEvHeXRpNCt',
			debug: false
		};

		var config = {
			endpoints: {
				api: "api.quickblox.com"
			},
			ssl: false,
			debug: true
		};

		// initalise the environmenet with my application id, authentication key and authentication secret
		QB.init(CONFIG.application_id , CONFIG.auth_key, CONFIG.authSecret, CONFIG.debug);

		// create an API session (user is authenticated)
		var params = {email: 'clientdeskhq@gmail.com', password: '@CD2015!0'};
		//var params = {provider: 'facebook', keys: {token: 'AM46dxjhisdffgry26282352fdusdfusdfgsdf'}};

		QB.createSession(params, function(err, result) { 
			if(err) {
				console.log(err);
			}
			else {
				console.log(result);

				var counter = 0;

				function submitQBImages(){
				
					if (imageDetailsArray[counter]){
						// convert the imageData into a Blob
						var byteCharacters = atob(imageDetailsArray[counter]);
						var byteNumbers = new Array(byteCharacters.length);

						for (var i = 0; i < byteCharacters.length; i++) {
							byteNumbers[i] = byteCharacters.charCodeAt(i);
						}
						
						var byteArray = new Uint8Array(byteNumbers);
						var contentType = "image/jpeg";
						var imageBlob = new Blob([byteArray], {type: contentType});

						QB.content.createAndUpload({ file: imageBlob,  name: "File", type: "image/jpeg", public: true}, function(err, response){
							if(err) {
								console.log(err);
							}
							else {
								console.log(response);
								console.log("http://qbprod.s3.amazonaws.com/" + response.uid);

								//submit quickblox data to API
								imageResponse  = {
									type: "Item Image",
									responseuid : response.uid,
									userid: "54e41a749b0f1e0e02b1b63b",
									name: itemName + "client1" + "itemImage"+counter,
									filetype: "image/jpeg",
									url : "http://qbprod.s3.amazonaws.com/"+response.uid,
									quickbloxid : response.quickbloxid,
									quickbloxuid : response.quickbloxuid
								};

								submitedImagesArray.push(imageResponse);

								counter++;
								submitQBImages();
							}
						}); // done createAndUpload
					}
					else{
						itemName = document.getElementById('itemname').value;
						itemBrand = document.getElementById('itembrand').value;
						itemPrice = document.getElementById('itemprice').value;
						itemDate = document.getElementById('itemdate').value;
						itemModelNo = document.getElementById('itemmodel').value;
						
						itemObject = {
							clientid: userid,
							category: $state.params.inventoryCategory,
							name: itemName,
							brand: itemBrand,
							price: itemPrice,
							dateofpurchase: itemDate,
							modelno: itemModelNo, 
							imagedetails: submitedImagesArray,
							policynumber: policynumber
						};

						$scope.apiCall('POST', '/inventory', itemObject, true);
						$ionicLoading.hide();
						$scope.goBack();
					}
				};
				
				submitQBImages();
			} // done else
		}); // end of quickblox code
	};

	function checkFields(){
		var noOfImages = 0;
		
		if(itemImage1 == null){
			console.log("Null itemImage1");
		}else{
			noOfImages++;
		}
		
		if(itemImage2 == null){
			console.log("Null itemImage2");
		}else{
			noOfImages++;
		}

		if(receiptImage == null){
			console.log("Null receipt");
		}else{
			noOfImages++;
		}

		if(noOfImages > 0){
			console.log("No of images" + noOfImages);
			return true;
		}
		else{
			$ionicLoading.hide();
			var alertPopup = $ionicPopup.alert({
				title: 'Error',
				template: 'Please attach atleast One Image'
			});
			alertPopup.then(function(res) {
				//console.log('Thank you for not eating my delicious ice cream cone');
			});
			return false;
		}
	}

	$scope.saveItem = function(){
		
		$ionicLoading.show({
			template: 'Saving...'
		});

		if(checkFields()){ 
			if(itemImage1){
				imageDetailsArray.push(itemImage1);
			}
		
			if(itemImage2){
				imageDetailsArray.push(itemImage2);
			}
		
			if(receiptImage){
				imageDetailsArray.push(receiptImage);
			}

			$scope.submitImageFiles();
		}
	};

	$scope.takePicture = function() {
		var cameraOptions = {
			quality : 100, 
			destinationType : Camera.DestinationType.DATA_URL, 
			sourceType : Camera.PictureSourceType.CAMERA, 
			allowEdit : true,
			encodingType: Camera.EncodingType.JPEG,
			targetWidth: 300,
			targetHeight: 300,
			popoverOptions: CameraPopoverOptions,
			saveToPhotoAlbum: true
		}

		navigator.camera.getPicture(function(imageData) {

			$scope.picData = imageData;

			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			$scope.$apply();

			var img = document.getElementById(clickedButtonId);
			img.src = $scope.imgURI;

			if(clickedButtonId == "image1"){
				itemImage1 = imageData;
			}
			if(clickedButtonId == "image2"){
				itemImage2 = imageData;
			}
			if(clickedButtonId == "image3"){
				receiptImage = imageData;
			}

			$scope.closeModal(1);
		
		}, function(err) {
			// alert(err);
			navigator.notification.alert("Sorry, an error has occurred", $scope.alertAutoDismissed(), "Error", "OK");
			$scope.closeModal(1);
		}, cameraOptions );
	};

	$scope.selectPicture = function() {
		var cameraOptions = {
			quality : 100, 
			destinationType : Camera.DestinationType.DATA_URL, 
			//destinationType : Camera.DestinationType.FILE_URI, 
			sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
			allowEdit : true,
			encodingType: Camera.EncodingType.JPEG,
			//targetWidth: 300,
			//targetHeight: 300,
			//popoverOptions: CameraPopoverOptions,
			saveToPhotoAlbum: false
		}

		navigator.camera.getPicture(function(imageData) {
		
			$scope.picData = imageData;
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			$scope.$apply();

			//var img = document.getElementById("image");
			var img = $scope.getAvailableImageTag();
			img.src = $scope.imgURI;
			
			if(clickedButtonId == "image1"){
				itemImage1 = imageData;
			}
			if(clickedButtonId == "image2"){
				itemImage2 = imageData;
			}
			if(clickedButtonId == "image3"){
				receiptImage = imageData;
			}

			$scope.closeModal(1);

		}, function(err) {
			// alert(err);
			navigator.notification.alert("Sorry, an error has occurred", $scope.alertAutoDismissed(), "Error", "OK");
			$scope.closeModal(1);
		}, cameraOptions );
	};

	$scope.getAvailableImageTag = function() {
		
		//check what image object is available
		var img = document.getElementById(clickedButtonId);
		return img;
	};

	$scope.displayImageFullScreen = function(index) {

		var imgSrc = document.getElementById("image"+index).src;
		var imageBase64JPEG = imgSrc.substring(23);
		//window.FullScreenImage.showImageBase64
		FullScreenImage.showImageBase64(imageBase64JPEG, 'Image'+index, 'jpg');
	};

	$scope.showDetails = function(index) {

		//check if image index exits
		clickedButtonId = "image"+index;
		var imgSrc = document.getElementById("image"+index).src
		if (!imgSrc || imgSrc.indexOf("mediaCapIcon@2x.png") > -1) {
			$scope.openModal(1);
			return;
		}

		var options = {
			//title: '',
			buttonLabels: ['View'],
			addCancelButtonWithLabel: 'Cancel',
			androidEnableCancelButton : true,
			winphoneEnableCancelButton : true,
			addDestructiveButtonWithLabel : 'Delete'
		};

		document.addEventListener("deviceready", function () {
			$cordovaActionSheet.show(options)
				.then(function(btnIndex) {
					var btnIndex = btnIndex;
					switch(btnIndex) {
						case 1:
							$scope.deleteConfirmation(index);
							break;
						case 2:
							$scope.displayImageFullScreen(index);
							break;
					}
				});
		}, false);
	};

	$scope.deleteConfirmation = function() {
		navigator.notification.confirm("Are you sure you want to delete image?", function(buttonIndex) {
			switch(buttonIndex) {
				case 1:
					console.log("Cancel");
					break;
				case 2:
					console.log("Delete");
					$scope.deleteImage();
					break;
			}
		}, "Delete image", [ "Cancel", "Ok" ]);
	};

	$scope.deleteImage = function() {
		document.getElementById(clickedButtonId).src = "img/mediaCapIcon@2x.png";
		if(clickedButtonId == "image1"){
			itemImage1 = null;
		}
		else if(clickedButtonId == "image2"){
			itemImage2 = null;
		}
		else{
			receiptImage = null;
		}
	};

	$scope.goBack = function () {
		$ionicHistory.goBack();
		// location.href="#/app/myinventory";
	};

	$scope.apiCall = function (methodType, resource, data, isAsync) { 
		// var apiURL = 'http://127.0.0.1:3000/api';
		// var apiURL = 'http://192.168.10.104:3000/api';
		var apiURL = 'http://dashboard.clientdesk.co:3000/api';
		
		var apiResource = resource;
		var apiURI  = apiURL + apiResource;

		var credentials = {
			id: 'core',
			key: 'password',
			algorithm: 'sha256'
		}

		var requestOptions = {
			uri: apiURI,
			method: methodType,
			headers: {}
		};

		var header = hawk.client.header(apiURI, methodType, { credentials: credentials, ext: 'some-app-data' });
		requestOptions.headers.Authorization = header.field;

		var responseJSON = null;

		$.ajaxSetup({
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
				xhr.setRequestHeader('Authorization', requestOptions.headers.Authorization);
			}
		});

		// Request with custom header
		$.ajax({
			async: isAsync,
			type: methodType,
			url: apiURI,
			success: function(response){
				responseJSON = JSON.stringify(response);
				var alertPopup = $ionicPopup.alert({
					title: 'Success',
					template: 'Item saved successfully'
				});
				alertPopup.then(function(res) {
					$scope.goBack();
				});
			},
			error: function(response) {
				//responseJSON = JSON.stringify(response);
				// alert(JSON.stringify(response));
				navigator.notification.alert("Sorry, an error has occurred", $scope.alertAutoDismissed(), "Error", "OK");
			},
			data: JSON.stringify(data),
			dataType: 'json'
		});
	};
});