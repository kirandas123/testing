angular.module('LoginController', ['ionic', 'ui.router'])

.controller('LoginCtrl', function($scope, $ionicPopup, $http, $state, $stateParams, $location, $ionicHistory, $cordovaInAppBrowser, $ionicLoading) {

	$scope.doLogin = function() {

		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		try{
			var oUser = {
				username: $scope.loginData.username,
				password: $scope.loginData.password
			};

			$scope.apiCallLogin('POST', '/user/login/', oUser, true);

			$scope.alertDismissed();
		}

		catch(err){
			navigator.notification.alert("Please, insert your username and password", $scope.alertDismissed(), "Login", "OK");
		}
	};

	$scope.alertDismissed = function() {
		
		$ionicHistory.nextViewOptions({
			disableAnimate: false,
			disableBack: true
		});
		
		$ionicHistory.clearHistory();

		$ionicLoading.hide();
	};

	/* ***************************************** */
	/*                 API Calls                 */
	/* ***************************************** */
	$scope.apiCallLogin = function (methodType, resource, data, isAsync) { 
		// var apiURL = 'http://127.0.0.1:3000/api';
		// var apiURL = 'http://192.168.10.104:3000/api';
		var apiURL = 'http://dashboard.clientdesk.co:3000/api';
		
		var apiResource = resource;
		var apiURI  = apiURL + apiResource;

		var credentials = {
			id: 'core',
			key: 'password',
			algorithm: 'sha256'
		};

		var requestOptions = {
			uri: apiURI,
			method: methodType,
			headers: {}
		};

		var header = hawk.client.header(apiURI, methodType, { credentials: credentials, ext: 'some-app-data' });
		
		requestOptions.headers.Authorization = header.field;

		$.ajaxSetup({
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
				xhr.setRequestHeader('Authorization', requestOptions.headers.Authorization);
			}
		});

		// Request with custom header
		$.ajax({
			async: isAsync,
			type: methodType,
			url: apiURI,
			success: function(response){

				if(response.error){
					navigator.notification.alert(response.error, $scope.alertDismissed(), "Login", "OK");
				}
				else{
					window.localStorage['loggedUser'] = JSON.stringify(response);
					$state.go('app.dashboard', {});
				}
			},
			error: function(response) {
				console.log(response);
			},
			data: JSON.stringify(data),
			dataType: 'json'
		});		
	};
});