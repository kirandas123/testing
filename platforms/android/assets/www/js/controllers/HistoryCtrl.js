angular.module('HistoryController', ['ionic','ui.router'])

.controller('HistoryCtrl', function($scope, ChatHistory, $state, $stateParams, $timeout, $window) {
	
	ChatHistory.fetchSessions();
	console.log(ChatHistory.all());
	$scope.histories = ChatHistory.all();

	$scope.openChatHistory = function (key, status, sessionDepartment) {
		$state.go('chat-history', {
			sessionKey: key,
			sessionStatus: status,
			department: sessionDepartment
		});
	}
});