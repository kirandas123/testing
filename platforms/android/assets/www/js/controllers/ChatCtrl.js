angular.module('ChatController', ['ionic','ui.router'])

.controller('ChatCtrl',function ($scope, Requests, Chats, $state, $timeout, $window, $ionicPopup){

	console.log("Chat Controller initialized");
	console.log("Sended request : " + $state.params.requestID);

	var ref;
	var sessionStatus;
	var roomName;
	var exitSession = false;
	$scope.IM = {
		textMessage: ""
	};

	setSession();
	var myVar=setInterval(function () {myTimer()}, 20000);

	function myTimer() {
		//var d = new Date();
		//document.getElementById("demo").innerHTML = d.toLocaleTimeString();
		if(roomName == null || roomName == ""){
			window.clearInterval(myVar);
			Requests.removeRequest($state.params.requestID);
			exitSession = true;

			var confirmPopup = $ionicPopup.confirm({
				title: 'Sorry! We are not available at the moment',
				template: 'Would you like to make a call request',
			});
			confirmPopup.then(function(res) {
				if(res) {
					console.log('Make call request');
						Requests.makeCallRequest($state.params.clientid, $state.params.clientname, $state.params.department);
						$window.history.back();
				} else {
					console.log('Cancel');
					$window.history.back();
				}
			});
		}
		else{
			window.clearInterval(myVar);
		}
	};

	function setSession(){
		Chats.setChatSession($state.params.requestID);

		setTimeout(function () {
			roomName = "";
			roomName = Chats.getSelectedRoomName();
			console.log("Roomname in ChatCtrl - " + roomName);

			if(!exitSession){
				if (roomName) {
					window.clearInterval(myVar);
					var cancelRequestButton=document.getElementById("cancelRequestButton");
					cancelRequestButton.parentNode.removeChild(cancelRequestButton);
					//cancelRequest.disabled  = true;
					$scope.roomName = " - " + roomName;
					$scope.chats = Chats.all();
					$scope.$apply();
					ref = new Firebase("https://clientdesk.firebaseio.com/sessions/" + roomName + "/");

					ref.on('child_changed', function(childSnapshot, prevChildName) {
						sessionStatus = "";

						if(childSnapshot.key() == "status" && childSnapshot.val() != "open"){
							// var sendMessageText = document.getElementById('messageTextInput');
							if (childSnapshot.val() == "closed" || childSnapshot.val() == "closed by client" || childSnapshot.val() == "temporarily closed") {
								var messageTextInput=document.getElementById("messageTextInput");
								messageTextInput.parentNode.removeChild(messageTextInput);
								var sendButton=document.getElementById("sendButton");
								sendButton.parentNode.removeChild(sendButton);

								var messageTextInputDiv=document.getElementById("messageTextInputDiv");
								sessionStatus = "Session is closed"
								var statusText = document.createTextNode(sessionStatus);
								messageTextInputDiv.appendChild(statusText);
							}
							else{
								setSession($state.params.requestID);
								$window.location.reload(true);
							}
						}
					});
				}
				else{
					setSession();
				}
			}
		}, 1000);
	};

	$scope.sendMessage = function (msg) {
		console.log(msg);

		//var authData = JSON.parse(window.localStorage['authData']);
		//console.log(authData);

		var oClient = JSON.parse(window.localStorage['loggedUser']);

		var clientName = oClient.firstname + " " + oClient.lastname;

		var user = {
			//displayName : authData.password.email
			displayName : clientName
		};

		//console.log(firebase);
		Chats.send(user, msg);

		$scope.IM.textMessage = "";
	};

	// $scope.remove = function (chat) {
	//     Chats.remove(chat);
	// }

	$scope.cancelRequest = function () {

		Requests.removeRequest($state.params.requestID);
		exitSession = true;
		window.clearInterval(myVar);
		$window.history.back();

	};

	$scope.quitSession = function () {
		if (sessionStatus == "Session is closed") {
			$window.history.back();
		}
		else{
			ref.update({
				status: "closed by client"
			});
			$window.history.back();
		}
	};
});