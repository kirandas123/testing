angular.module('MediacaptureController', ['ionic','ui.router'])

.controller('MediacaptureCtrl', function($scope, $cordovaCapture, $state, $ionicModal, $ionicPopup, $cordovaActionSheet, $ionicHistory, $cordovaSQLite) {

	var audioObject;

	//Media Capture - Chooser:
	$ionicModal.fromTemplateUrl('templates/mediacaptuer-choose.html', {
		id: '1',
		scope: $scope,
		animation: 'fade',
		backdropClickToClose: true
	}).then(function(modal) {
		$scope.oModal1 = modal;
	});

	// Open + Close Modals
	$scope.openModal = function(index) {
		if(index == 1) 
			$scope.oModal1.show();
	};

	$scope.closeModal = function(index) {
		if(index == 1) 
			$scope.oModal1.hide();
	};

	/* Listen for broadcasted messages */
	$scope.$on('modal.shown', function(event, modal) {
		// console.log('Modal ' + modal.id + ' is shown!');
	});

	$scope.$on('modal.hidden', function(event, modal) {
		// console.log('Modal ' + modal.id + ' is hidden!');
	});

	$scope.$on('$destroy', function() {
		// console.log('Destroying modals...');
		$scope.oModal1.remove();
	});

	$scope.takePicture = function() {
		var cameraOptions = {
			quality : 100, 
			destinationType : Camera.DestinationType.DATA_URL, 
			sourceType : Camera.PictureSourceType.CAMERA, 
			allowEdit : true,
			encodingType: Camera.EncodingType.JPEG,
			targetWidth: 300,
			targetHeight: 300,
			popoverOptions: CameraPopoverOptions,
			saveToPhotoAlbum: true
		}

		navigator.camera.getPicture(function(imageData) {
			$scope.picData = imageData;
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			$scope.$apply();

			var img = document.getElementById("image");
			img.src = $scope.imgURI;
			$scope.closeModal(1);
		}, function(err) {
			// alert(err);
			navigator.notification.alert("Sorry, an error has occurred", $scope.alertAutoDismissed(), "Error", "OK");
			$scope.closeModal(1);
		}, cameraOptions );
	};

	$scope.selectPicture = function() {
		var cameraOptions = {
			quality : 100, 
			destinationType : Camera.DestinationType.DATA_URL, 
			//destinationType : Camera.DestinationType.FILE_URI, 
			sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
			allowEdit : true,
			encodingType: Camera.EncodingType.JPEG,
			//targetWidth: 300,
			//targetHeight: 300,
			//popoverOptions: CameraPopoverOptions,
			saveToPhotoAlbum: false
		}

		navigator.camera.getPicture(function(imageData) {
			$scope.picData = imageData;
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			$scope.$apply();

			//var img = document.getElementById("image");
			var img = $scope.getAvailableImageTag();
			img.src = $scope.imgURI;
			$scope.closeModal(1);
		}, function(err) {
			// alert(err);
			navigator.notification.alert("Sorry, an error has occurred", $scope.alertAutoDismissed(), "Error", "OK");
			$scope.closeModal(1);
		}, cameraOptions );
	};

	$scope.recordVideo = function() {
	
		var options = { limit: 3, duration: 15 };

		$cordovaCapture.captureVideo(options).then(function(videoData) {
			// Success! Video data is here
			var i, path, len;
			
			for (i = 0, len = videoData.length; i < len; i += 1) {
				path = videoData[i].fullPath;
				// do something interesting with the file
			}
				//document.getElementById("myVideo").src = path
				document.getElementById("myVideo").href = path
		}, function(err) {
			// An error occurred. Show a message to the user
			navigator.notification.alert('Error code: ' + error.code, null, 'Capture Error');
		});
	};

	$scope.playVideo = function() {

		var videoUrl = document.getElementById("myVideo").href;

		// Just play a video
		// window.plugins.streamingMedia.playVideo(videoUrl);

		// Play a video with callbacks
		var options = {
			successCallback: function() {
				console.log("Video was closed without error.");
			},
			errorCallback: function(errMsg) {
				console.log("Error! " + errMsg);
			}
		};
		window.plugins.streamingMedia.playVideo(videoUrl, options);
	};

	$scope.recordAudio = function() {
	
		var options = { limit: 3, duration: 10 };

		$cordovaCapture.captureAudio(options).then(function(audioData) {
			
			var i, path, len;
			
			for (i = 0, len = audioData.length; i < len; i += 1) {
				path = audioData[i].fullPath;
			}

			document.getElementById("myAudio").src = path;

			audioObject = audioData[0];

		}, function(err) {
			// An error occurred. Show a message to the user
			navigator.notification.alert('Error code: ' + error.code, null, 'Capture Error');
		});
	};

	$scope.playAudio = function() {

		var audioUrl = document.getElementById("myAudio").src;

		// Play an audio file (not recommended, since the screen will be plain black)
		//window.plugins.streamingMedia.playAudio(audioUrl);

		// Play an audio file with options (all options optional)
		var options = {
			successCallback: function() {
				console.log("Player closed without error.");
			},
			errorCallback: function(errMsg) {
				console.log("Error! " + errMsg);
			}
		};

		window.plugins.streamingMedia.playAudio(audioUrl, options);
	};

	$scope.getAvailableImageTag = function() {
	
		//check what image object is available
		var img;
		var numberOfImages = 9;

		for (i=1; i<= numberOfImages; i++) {
			if ( i == 1 ) {
				var imgSrc = document.getElementById("image1").src;
				if (imgSrc.indexOf("mediaCapIcon@2x.png") > -1) {
					//remove camera icon and reset the img html
					document.getElementById("image1").remove();
					document.getElementById("button1").innerHTML = '<img id="image1" style="height:100%; width:auto"/>';   
				}
			}
			if ( !document.getElementById("image"+i).src ) {
				// check if the current image already contains a src
				document.getElementById("button"+i).style.padding = '0';
				document.getElementById("button"+i).innerHTML = '<img id="image'+i+'" style="height:100%; width:auto"/>';
				img = document.getElementById("image"+i);
				break;
			}
		}

		return img;
	};

	$scope.displayImageFullScreen = function(index) {

		var imgSrc = document.getElementById("image"+index).src;
		var imageBase64JPEG = imgSrc.substring(23);

		//window.FullScreenImage.showImageBase64
		FullScreenImage.showImageBase64(imageBase64JPEG, 'Image'+index, 'jpg');
	};

	$scope.showDetails = function(index) {

		//check if image index exits
		var imgSrc = document.getElementById("image"+index).src
		if (!imgSrc || imgSrc.indexOf("mediaCapIcon@2x.png") > -1) {
			$scope.openModal(1);
			return;
		}

		var options = {
			//title: '',
			buttonLabels: ['View'],
			addCancelButtonWithLabel: 'Cancel',
			androidEnableCancelButton : true,
			winphoneEnableCancelButton : true,
			addDestructiveButtonWithLabel : 'Delete'
		};

		document.addEventListener("deviceready", function () {

			$cordovaActionSheet.show(options)
				.then(function(btnIndex) {
					var btnIndex = btnIndex;

					switch(btnIndex) {
						case 1:
							$scope.deleteConfirmation(index);
							break;
						case 2:
							$scope.displayImageFullScreen(index);
							break;
					}
			});
		}, false);
	};

	$scope.deleteConfirmation = function(index) {
		navigator.notification.confirm("Are you sure you want to delete image"+index+"?", function(buttonIndex) {
			switch(buttonIndex) {
				case 1:
					console.log("Cancel");
					break;
				case 2:
					console.log("Delete");
					$scope.deleteImage(index);
					break;
			}
		}, "Delete image", [ "Cancel", "Ok" ]);
	};

	$scope.deleteImage = function(index) {
		document.getElementById("button"+index).innerHTML = '<img id="image'+index+'" style="height:100%; width:auto"/>';

		//fix the positioning of the images
		var numberOfImages = 9;
		var deletedImageIndex = index;
		var nextImageIndex = deletedImageIndex+1;

		for (i=nextImageIndex; i<= numberOfImages; i++) {
			var nextImgSrc = document.getElementById("image"+i).src;

			if(!nextImgSrc){
				//next image is empty
				break;
			}
			else {
				// next image is not empty so swap them
				document.getElementById("image"+(i-1)).src = document.getElementById("image"+i).src
				// remove the swapped image 
				document.getElementById("button"+i).innerHTML = '<img id="image'+i+'" style="height:100%; width:auto"/>';
			}
		}
	};

	function removeBase64(input) {
		input = input.slice(23, input.length);
		return input;
	};

	$scope.goBack = function () {
		$ionicHistory.goBack();
	};

	$scope.getFiles = function(){

		// Save Images to SQLite
		var query1 = "DELETE FROM claimedImages";
		$cordovaSQLite.execute(db, query1).then(function(res) {
			
			var images = [];
			var counter = 0;

			for (var i = 1; i < 10; i++){
				try{
					var image = document.getElementById("image"+i).src;
					
					if (image) {
						var result = image.indexOf("mediaCapIcon@2x") > -1;
						if (result == false){
							images.push(image);
						}
					}
				}
				catch(err){
					// show error
				}
			};

			function insertImages(){
				if (images[counter]){
					var imageClean = removeBase64(images[counter]);
					var query = "INSERT INTO claimedImages (imageData) VALUES (?)";
					$cordovaSQLite.execute(db, query, [imageClean]).then(function(res) {
						counter++
						insertImages();
					}, function (err) {
						 console.error(err);
					});
				}
			}

			if (images.length > 0){
				insertImages();
			}

		}, function (err) {
			// console.log("Some Error");
			console.error(err);
		});

		// Save Audios to SQLite
		var query2 = "DELETE FROM claimedAudios";
		$cordovaSQLite.execute(db, query2).then(function(res) {
			
			if (audioObject){
				audioObject = JSON.stringify(audioObject);
				insertAudio();
			}

			function insertAudio(){
				var query = "INSERT INTO claimedAudios (audioData) VALUES (?)";
				$cordovaSQLite.execute(db, query, [audioObject]).then(function(res) {
					// success
				}, function (err) {
					console.error(err);
				});
			}
		}, function (err) {
			// console.log("Some Error");
			console.error(err);
		});

		// var query3 = "DELETE FROM claimedVideos";
		// $cordovaSQLite.execute(db, query3).then(function(res) {
		// 	try{
		// 		var video = document.getElementById("myVideo").src;
		// 		if (video){
		// 			insertVideo();
		// 		}
		// 	}
		// 	catch(err){
		// 		// show error
		// 	}

		// 	function insertVideo(){
		// 		var query = "INSERT INTO claimedVideos (videoData) VALUES (?)";
		// 		$cordovaSQLite.execute(db, query, [video]).then(function(res) {
		// 			// success
		// 		}, function (err) {
		// 			console.error(err);
		// 		});
		// 	}
		// }, function (err) {
		// 	// console.log("Some Error");
		// 	console.error(err);
		// });
	};
	
	$scope.alertAutoDismissed = function() {
		ActivityIndicator.hide();
	};

	$scope.goNext = function() {

		$scope.getFiles();

		if (window.localStorage['claimType'] == "auto"){
			$state.go('app.attachinventoryitems', {});
		}
		if (window.localStorage['claimType'] == "property"){
			$state.go('app.geodata', {});
		}
	};
});