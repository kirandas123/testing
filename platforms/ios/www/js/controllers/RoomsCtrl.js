angular.module('RoomsController', ['ionic','ui.router'])

.controller('RoomsCtrl', function ($scope, Requests, $state) {
	
	//HardwareBackButtonManager.disable();
	// alert("RoomsCtrl");
	
	$scope.quitToDashBoard = function(){
		location.href='#/dashboard';
	};
	
	$scope.requestChat = function (department, type) {

		var oClient = JSON.parse(window.localStorage['loggedUser']);

		var clientName = oClient.firstname + " " + oClient.lastname;

		var reqId = Requests.sendRequest(oClient.clientid, clientName, department, type, '0');

		$state.go('chat', {
			requestID: reqId,
			department: department,
			clientname: clientName,
			clientid: oClient.clientid
		});
	};
});