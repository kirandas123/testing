angular.module('ClaimsController', ['ionic','ui.router'])

.controller('ClaimsCtrl', function($scope, $ionicModal, $ionicPopup, $cordovaGeolocation, $http, $state, $stateParams, $ionicHistory, $cordovaInAppBrowser, $ionicLoading, $location, $cordovaSQLite) {
	
	var userid = JSON.parse(window.localStorage['loggedUser']).clientid;

	$scope.claimtypes = [
		{ title: 'Vehicle Collision', id: 1, type: 'auto' },
		{ title: 'Windshield Damage', id: 2, type: 'auto' },
		{ title: 'Hit-and-Run', id: 3, type: 'auto' },
		{ title: 'Theft', id: 4, type: 'auto' },
		{ title: 'Hail', id: 5, type: 'auto' },
		{ title: 'Loss of Control', id: 6, type: 'auto' },
		{ title: 'Disaster', id: 7, type: 'auto' },
		{ title: 'Other', id: 8, type: 'auto' },
		{ title: 'Home Break-in', id: 9, type: 'property' },
		{ title: 'Water Damage', id: 10, type: 'property' },
		{ title: 'Fire Damage', id: 11, type: 'property' },
		{ title: 'Vandalism', id: 12, type: 'property' },
		{ title: 'Roof Damage', id: 13, type: 'property' },
		{ title: 'Disaster', id: 14, type: 'property' },
		{ title: 'Other', id: 15, type: 'property' }
	];

	$scope.ownershipList = [
		{ text: "Personal", checked: false },
		{ text: "Rental", checked: false }
	];

	$scope.disasterList = [
		{ title: "Earthquake" },
		{ title: "Hurricane" },
		{ title: "Tornado" },
		{ title: "Tsunami" }
	];

	$scope.roomsList = [
		{ checked: false, title: "Attic" },
		{ checked: false, title: "Basement" },
		{ checked: false, title: "Bedroom" },
		{ checked: false, title: "Gym" },
		{ checked: false, title: "Kitchen" },
		{ checked: false, title: "Laundry room" },
		{ checked: false, title: "Library" },
		{ checked: false, title: "Living room" },
		{ checked: false, title: "Office" },
		{ checked: false,  title: "Storage" }
	];

	$scope.areasList = [
		{ checked: false, title: "Front" },
		{ checked: false, title: "Side" },
		{ checked: false, title: "Garage" },
		{ checked: false, title: "Backyard" },
		{ checked: false, title: "Swimming pool" },
		{ checked: false, title: "Shed" }
	];

	// Driver modal
	$ionicModal.fromTemplateUrl('templates/claimitems-driver.html', {
		id: '1',
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true,
		backdropClickToClose: false
	}).then(function(modal) {
		$scope.oModal1 = modal;
	});

	// Vehicle modal
	$ionicModal.fromTemplateUrl('templates/claimitems-vehicle.html', {
		id: '2',
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true,
		backdropClickToClose: false
	}).then(function(modal) {
		$scope.oModal2 = modal;
	});

	// Passenger modal
	$ionicModal.fromTemplateUrl('templates/claimitems-passenger.html', {
		id: '3',
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true,
		backdropClickToClose: false
	}).then(function(modal) {
		$scope.oModal3 = modal;
	});

	// Witness modal
	$ionicModal.fromTemplateUrl('templates/claimitems-witness.html', {
		id: '4',
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true,
		backdropClickToClose: false
	}).then(function(modal) {
		$scope.oModal4 = modal;
	});

	// Police modal
	$ionicModal.fromTemplateUrl('templates/claimitems-police.html', {
		id: '5',
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true,
		backdropClickToClose: false
	}).then(function(modal) {
		$scope.oModal5 = modal;
	});

	// Geo modal
	$ionicModal.fromTemplateUrl('templates/claimitems-geo.html', {
		id: '6',
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true,
		backdropClickToClose: false
	}).then(function(modal) {
		$scope.oModal6 = modal;
	});

	// Open + Close Modals
	$scope.openModal = function(index) {
		if(index == 1) 
			$scope.oModal1.show();
		if(index == 2) 
			$scope.oModal2.show();
		if(index == 3) 
			$scope.oModal3.show();
		if(index == 4) 
			$scope.oModal4.show();
		if(index == 5) 
			$scope.oModal5.show();
		if(index == 6) 
			$scope.oModal6.show();
	};

	$scope.closeModal = function(index) {
		if(index == 1) 
			$scope.oModal1.hide();
		if(index == 2) 
			$scope.oModal2.hide();
		if(index == 3) 
			$scope.oModal3.hide();
		if(index == 4) 
			$scope.oModal4.hide();
		if(index == 5) 
			$scope.oModal5.hide();
		if(index == 6) 
			$scope.oModal6.hide();
	};

	//Geo call backs
	$scope.saveGeodata = function() {
		$scope.markAsDone(6);
		$scope.closeModal(6);
	};

	$scope.changeOwnership = function(item){
		if (item.text == "Personal")
			$scope.ownershipList[1].checked = false;
		if (item.text == "Rental")
			$scope.ownershipList[0].checked = false;

		window.localStorage['typeOfOwnership'] = JSON.stringify(item);
	};

	function getGeodata(){
		ionic.Platform.ready(function() {
			//write time and date
			var dCurrentDate = moment();
			var sFormattedDate = moment(dCurrentDate).format('h:mm a - MMMM Do, YYYY');

			window.localStorage['timestamp'] = dCurrentDate;
			document.getElementById("timestamp").value = sFormattedDate;

			var posOptions = {timeout: 5000, enableHighAccuracy: true};

			//your code to be executed after 1 seconds
			$cordovaGeolocation
				.getCurrentPosition(posOptions)
				.then(function (position) {
					
					var lat  = position.coords.latitude;
					var long = position.coords.longitude;
					var requestString = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + long;

					$http.get(requestString)
						.success(function(data, status, headers, config) {
		
							// this callback will be called asynchronously
							// when the response is available
							var oResults = data;

							var sStreetNumber = oResults.results[0].address_components[0].long_name;
							var sStreetName = oResults.results[0].address_components[1].short_name;
							var sCity = oResults.results[0].address_components[3].short_name;
							var sProvice = oResults.results[0].address_components[6].short_name;
							var sCountry = oResults.results[0].address_components[7].long_name;


							var sFormattedAddress = oResults.results[0].formatted_address;
							var sLat = oResults.results[0].geometry.location.lat;
							var sLong = oResults.results[0].geometry.location.lng;

							sLat = sLat.toString();
							sLong = sLong.toString();

							var geodetails = {
								address : sFormattedAddress,
								city : sCity,
								provice : sProvice,
								latitude : sLat,
								longitude : sLong
							}

							document.getElementById("location").value = geodetails.address;
							//save geolocation to local storage
							window.localStorage['geodetails'] = JSON.stringify(geodetails);

							getWeather(geodetails);

						})
						.error(function(data, status, headers, config) {
							// called asynchronously if an error occurs
							// or server returns response with an error status.
							navigator.notification.alert("Sorry, an error has occurred.", $scope.alertAutoDismissed(), "Error", "OK");
							// alert(data);
						});
				}, function(err) {
					// error
					// alert(JSON.stringify(err));
					navigator.notification.alert("Sorry, an error has occurred. Please, make sure your location is active and try again.", $scope.alertAutoDismissed(), "Error", "OK");
				});

			function getWeather(geodetails) {
				//determinate weather conditions by calling openweather API
				var requestString = "http://api.openweathermap.org/data/2.5/weather?lat=" + geodetails.lat + "&lon=" + geodetails.long + "&units=metric";

				$http.get(requestString)
					.success(function(data, status, headers, config) {
						
						// this callback will be called asynchronously
						// when the response is available
						var oResults = data;
						var sWeather = oResults.weather[0].main;
						var sDescription = oResults.weather[0].description;
						var fTemp = oResults.main.temp;
						var sTemp = fTemp.toString();
						var aTemp = sTemp.split(".");

						if (aTemp[0] != null) {
							sTemp = aTemp[0] 
						}

						//build weather forecast string
						sWeather = sTemp + "° " + sWeather + " - " + sDescription; 

						//save weather to local storage
						window.localStorage['weatherdetails'] = sWeather;
						document.getElementById("weather").value = sWeather;
					})
					.error(function(data, status, headers, config) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
						// alert(data);
						navigator.notification.alert("Sorry, an error has occurred", $scope.alertAutoDismissed(), "Error", "OK");
					});
			};// end of $scope.getWeather()
		}); // end of ionic.Platform.ready
	};

	/* Listen for broadcasted messages */
	$scope.$on('modal.shown', function(event, modal){
		
		// console.log('Modal ' + modal.id + ' is shown!');
		var vendorCounter = 0;
		var vendors;

		if (modal.id == 1) {
			//load driver details from local storage
			if (window.localStorage['driverDetails'])
				$scope.loadDriverDetails();
		}
		if (modal.id == 2) {
			//load vehicle details from local storage
			if (window.localStorage['vehicleDetails'])
				$scope.loadVehicleDetails();
		}
		if (modal.id == 3) {
			//load passenger details from local storage
			if (window.localStorage['passengerDetails'])
				$scope.loadPassengerDetails();
		}
		if (modal.id == 4) {
			//load witness details from local storage
			if (window.localStorage['witnessDetails'])
				$scope.loadWitnessDetails();
		}
		if (modal.id == 5) {
			//load police details from local storage
			if (window.localStorage['policeDetails'])
				$scope.loadPoliceDetails();
		}
		if (modal.id == 6) {
			getGeodata();
		} //end if modal == 6
	}); // end of $scope.$on('modal.shown')

	$scope.$on('modal.hidden', function(event, modal) {
		// console.log('Modal ' + modal.id + ' is hidden!');
	});

	// Cleanup the modals when we're done with them (i.e: state change)
	// Angular will broadcast a $destroy event just before tearing down a scope 
	// and removing the scope from its parent.
	$scope.$on('$destroy', function() {
		// console.log('Destroying modals...');
		$scope.oModal1.remove();
		$scope.oModal2.remove();
		$scope.oModal3.remove();
		$scope.oModal4.remove();
		$scope.oModal5.remove();
		$scope.oModal6.remove();
	});

	$scope.markAsDone = function (index){
		switch(index) {
			case 1:
				document.querySelector("#driverDiv").className = "claimItem claimItemDone";
				break;
			case 2:
				document.querySelector("#vehicleDiv").className = "claimItem claimItemDone";
				break;
			case 3:
				document.querySelector("#passengersDiv").className = "claimItem claimItemDone";
				break;
			case 4:
				document.querySelector("#witnessesDiv").className = "claimItem claimItemDone";
				break;
			case 5:
				document.querySelector("#policeDiv").className = "claimItem claimItemDone";
				break;
			case 6:
				document.querySelector("#geodataDiv").className = "claimItem claimItemDone";
				break;
		}
	};
	
	$scope.getPolicies = function() {
		$scope.apiCall('GET', '/policies/' + userid, {}, true);
	};

	$scope.savePolicy = function(policy) {

		window.localStorage['selectedPolicy'] = JSON.stringify(policy);
		
		if (policy.type == "Automotive") {
			window.localStorage['claimType'] = "auto";
			$state.go('app.autoclaim', {cache: false});
		}
		if (policy.type == "Property") {
			window.localStorage['claimType'] = "property";
			$state.go('app.propertyclaim', {cache: false});
		}
	};

	$scope.typeOfClaimCategory = function (category) {

		window.localStorage['claimCategory'] = category;

		if (window.localStorage['claimType'] == "auto") {
			if (window.localStorage['typeOfOwnership']) {
				$state.go('app.claimitems', {cache: false});
			}
			else {
				navigator.notification.alert("Please, select Personal or Rental.", $scope.alertAutoDismissed(), "Error", "OK");
			}
		}
		if (window.localStorage['claimType'] == "property") {

			if (category == "Disaster") {
				$state.go('app.selectdisaster', {});
			}
			else if (category == "Other") {
				$state.go('app.addNotes', {});
			}
			else {
				$state.go('app.attachinventoryitems', {
					inventoryCategory: window.localStorage['claimType']
				});
			}
		}
	};

	$scope.typeOfDisaster = function(disaster) {
		window.localStorage['disasterType'] = disaster;
		// console.log(window.localStorage['disasterType']);
		$state.go('app.attachinventoryitems', {});
	};

	$scope.saveAffectedAreas = function() {
		
		var roomsArray = [];
		var areasArray = [];
		
		for (i=0; i < $scope.roomsList.length; i++) {
			if ($scope.roomsList[i].checked == true) {
				roomsArray.push($scope.roomsList[i].title);
			}
		}

		window.localStorage['affectedRooms'] = JSON.stringify(roomsArray);
		
		for(i=0; i < $scope.areasList.length; i++){
			if ($scope.areasList[i].checked == true){
				areasArray.push($scope.areasList[i].title);
			}
		}

		window.localStorage['affectedAreas'] = JSON.stringify(areasArray);

		$state.go('app.mediacapture', {});
	};

	$scope.getgeodetails = function(){
		getGeodata();
	};

	$scope.saveGeoDetails = function(){

		var category = String(window.localStorage['claimCategory']);

		if (category == "Home Break-in" || category == "Disaster" || category == "Vandalism" || category == "Fire Damage"){
			$state.go('app.policedetails', {});
		}
		else{
			$state.go('app.summary', {});
		}
	};

	// $scope.typeOfOwnership = function () {
	// 	// determine the type of auto ownership
	// 	window.localStorage['typeOfOwnership'] = $scope.ownershipList;
	// };

	// ============== DRIVER FUNCTIONS ==============
	$scope.saveDriverDetails = function () {

		var firstName = document.querySelector("#driverFirstName").value;
		var lastName = document.querySelector("#driverLastName").value;
		var policy = document.querySelector("#driverPolicy").value;
		var licensePlate = document.querySelector("#driverLicensePlate").value;
		var licenseNumber = document.querySelector("#driverLicenseNumber").value;
		var phoneNumber = document.querySelector("#driverPhone").value;
		//TODO: owner of vehicle

		if (firstName != "" && lastName != "" && policy != "" && licensePlate != "" && licenseNumber != "" && phoneNumber != ""){
			var driverDetails = {
				fname : firstName,
				lname : lastName,
				policy : policy,
				licenseplate : licensePlate,
				licensenumber : licenseNumber,
				phonenumber : phoneNumber
				//TODO: owner of vehicle
			};

			window.localStorage['driverDetails'] = JSON.stringify(driverDetails);

			// console.log('==================================');
			// console.log('Driver details');
			// console.log(JSON.stringify(driverDetails));

			$scope.closeModal(1);
			$scope.markAsDone(1);
		}
		else{
			navigator.notification.alert("Please, complete missing information", $scope.alertAutoDismissed(), "Driver Details", "OK");
		}
	};

	$scope.loadDriverDetails = function () {

		var driverDetails = JSON.parse(window.localStorage['driverDetails']);
		if (driverDetails) {
			document.querySelector("#driverFirstName").value = driverDetails.fname;
			document.querySelector("#driverLastName").value = driverDetails.lname;
			document.querySelector("#driverPolicy").value = driverDetails.policy;
			document.querySelector("#driverLicensePlate").value = driverDetails.licenseplate;
			document.querySelector("#driverLicenseNumber").value = driverDetails.licensenumber;
			document.querySelector("#driverPhone").value = driverDetails.phonenumber;
		}
	};

	$scope.submitDriverDetails = function (claimId){
		var oDriver = JSON.parse(window.localStorage['driverDetails']);
		oDriver.claimid = claimId;
		oDriver.vehicleowner = "yes";
		if (oDriver) {
			$scope.apiCall('POST', '/driver', oDriver, true);
		}
	};

	// ============== VEHICLE FUNCTIONS ==============
	$scope.saveVehicleDetails = function () {

		var typeSelect = document.querySelector("#vehicleTypeSelect");
		var type = typeSelect.options[typeSelect.selectedIndex].text;
		var make = document.querySelector("#vehicleMake").value;
		var model = document.querySelector("#vehicleModel").value;
		var color = document.querySelector("#vehicleColor").value;
		var year = document.querySelector("#vehicleYear").value;

		if (typeSelect != "" && type != "" && make != "" && model != "" && color != "" && year != ""){
			var vehicleDetails = {
				type : type,
				make : make,
				model : model,
				color : color,
				year : year
			};

			window.localStorage['vehicleDetails'] = JSON.stringify(vehicleDetails);

			// console.log(JSON.stringify(vehicleDetails));

			$scope.closeModal(2);
			$scope.markAsDone(2);
		}
		else{
			navigator.notification.alert("Please, complete missing information", $scope.alertAutoDismissed(), "Vehicle Details", "OK");
		}
	};

	$scope.loadVehicleDetails = function () {
		var vehicleDetails = JSON.parse(window.localStorage['vehicleDetails']);

		if (vehicleDetails) {
			//TODO: type select
			//document.querySelector("#type").value = vehicleDetails.type;
			document.querySelector("#vehicleMake").value = vehicleDetails.make;
			document.querySelector("#vehicleModel").value = vehicleDetails.model;
			document.querySelector("#vehicleColor").value = vehicleDetails.color;
			document.querySelector("#vehicleYear").value = vehicleDetails.year;
		}
	};

	$scope.submitVehicleDetails = function (claimId){
		var oVehicle = JSON.parse(window.localStorage['vehicleDetails']);
		oVehicle.claimid = claimId;
		if (oVehicle) {
			$scope.apiCall('POST', '/vehicle', oVehicle, true);
		}
	};

	// ============== PASSENGER FUNCTIONS ==============
	$scope.savePassengerDetails = function () {

		var firstName = document.querySelector("#passengerFirstName").value;
		var lastName = document.querySelector("#passengerLastName").value;
		var phoneNumber = document.querySelector("#passengerPhone").value;
		var email = document.querySelector("#passengerEmail").value;
		var locationSelect = document.querySelector("#passengerLocation");
		var location = locationSelect.options[locationSelect.selectedIndex].text;

		if (firstName != "" && lastName != "" && phoneNumber != "" && email != "" && locationSelect != "" && location != ""){
			var passengerDetails = {
				fname : firstName,
				lname : lastName,
				phone : phoneNumber,
				email : email,
				location : location          
			};

			window.localStorage['passengerDetails'] = JSON.stringify(passengerDetails);

			// console.log(JSON.stringify(passengerDetails));
			// console.log('==================================');
			// console.log('Passenger details');

			$scope.closeModal(3);
			$scope.markAsDone(3);
		}
		else{
			navigator.notification.alert("Please, complete missing information", $scope.alertAutoDismissed(), "Passenger Details", "OK");
		}
	};

	$scope.loadPassengerDetails = function () {
		var passengerDetails = JSON.parse(window.localStorage['passengerDetails']);

		if (passengerDetails) {
			document.querySelector("#passengerFirstName").value = passengerDetails.fname;
			document.querySelector("#passengerLastName").value = passengerDetails.lname;
			document.querySelector("#passengerPhone").value = passengerDetails.phone;
			document.querySelector("#passengerEmail").value = passengerDetails.email;
			//TODO: passenger location
		}
	};

	$scope.submitPassengerDetails = function (claimId){
		if (window.localStorage['passengerDetails']){
			var oPassenger = JSON.parse(window.localStorage['passengerDetails']);
			oPassenger.claimid = claimId;
			if (oPassenger) {
				$scope.apiCall('POST', '/passenger', oPassenger, true);
			}
		}
	};

	// ============== WITNESS FUNCTIONS ==============
	$scope.saveWitnessDetails = function () {

		var firstName = document.querySelector("#witnessFirstName").value;
		var lastName = document.querySelector("#witnessLastName").value;
		var phoneNumber = document.querySelector("#witnessPhone").value;
		var email = document.querySelector("#witnessEmail").value;
		var locationSelect = document.querySelector("#witnessLocation");
		var location = locationSelect.options[locationSelect.selectedIndex].text;

		if (firstName != "" && lastName != "" && phoneNumber != "" && email != "" && locationSelect != "" && location != ""){

			var witnessDetails = {
				fname : firstName,
				lname : lastName,
				phone : phoneNumber,
				email : email,
				location : location
			};

			window.localStorage['witnessDetails'] = JSON.stringify(witnessDetails);
			// console.log(JSON.stringify(witnessDetails));

			$scope.closeModal(4);
			$scope.markAsDone(4);
		}
		else{
			navigator.notification.alert("Please, complete missing information", $scope.alertAutoDismissed(), "Witness Details", "OK");
		}
	};

	$scope.loadWitnessDetails = function () {

		var witnessDetails = JSON.parse(window.localStorage['witnessDetails']);
		if (witnessDetails) {
			document.querySelector("#witnessFirstName").value = witnessDetails.fname;
			document.querySelector("#witnessLastName").value = witnessDetails.lname;
			document.querySelector("#witnessPhone").value = witnessDetails.phone;
			document.querySelector("#witnessEmail").value = witnessDetails.email;
			//TODO: witness location
		}
	};

	$scope.submitWitnessDetails = function (claimId){
		if (window.localStorage['witnessDetails']){
				var oWitness = JSON.parse(window.localStorage['witnessDetails']);
			oWitness.claimid = claimId;
			if (oWitness) {
				$scope.apiCall('POST', '/witness', oWitness, true);
			}
		}
	};

	// ============== POLICE FUNCTIONS ==============
	$scope.savePoliceDetails = function () {

		if (window.localStorage['claimType'] == "auto"){

			var lastName = document.querySelector("#policeLastName").value;
			var badgeNumber = document.querySelector("#policeBadgeNumber").value;
			var phoneNumber = document.querySelector("#policePhone").value;

			if (lastName != "" && badgeNumber != "" && phoneNumber != ""){
				var policeDetails = {
					name : lastName,
					badge : badgeNumber,
					phonenumber : phoneNumber
				};

				window.localStorage['policeDetails'] = JSON.stringify(policeDetails);
				// console.log(JSON.stringify(policeDetails));

				$scope.closeModal(5);
				$scope.markAsDone(5);
			}
			else{
				navigator.notification.alert("Please, complete missing information", $scope.alertAutoDismissed(), "Police Details", "OK");
			}
		}
		else{
			var lastName = document.querySelector("#policeLastName").value;
			var badgeNumber = document.querySelector("#policeBadgeNumber").value;
			var phoneNumber = document.querySelector("#policePhone").value;
			var policeReport = document.querySelector("#policeReport").value;

			var policeDetails = {
				name : lastName,
				badge : badgeNumber,
				phonenumber : phoneNumber,
				report: policeReport
			};
			
			window.localStorage['policeDetails'] = JSON.stringify(policeDetails);
			
			$state.go('app.summary', {});
		}
	};

	$scope.loadPoliceDetails = function () {
		var policeDetails = JSON.parse(window.localStorage['policeDetails']);

		if (policeDetails) {
			document.querySelector("#policeLastName").value = policeDetails.name;
			document.querySelector("#policeBadgeNumber").value = policeDetails.badge;
			document.querySelector("#policePhone").value = policeDetails.phonenumber;
		}
	};

	$scope.submitPoliceDetails = function (claimId){
		if (window.localStorage['policeDetails']){
			var oPolice = JSON.parse(window.localStorage['policeDetails']);
			oPolice.claimid = claimId;
			if (oPolice) {
				$scope.apiCall('POST', '/police', oPolice, true);
			}
		}
	};

	// ============== ADITIONAL FUNCTIONS ==============
	$scope.saveAditionalDetails = function () {

		var notes = document.querySelector("#notesTextarea").value;

		if (notes) {
			window.localStorage['claimNotes'] = notes;
			// console.log(notes);
		}
		else {
			notes = " ";
		}
	};

	$scope.loadAditionalDetails = function () {
		var notes = window.localStorage['claimNotes'];

		if (notes) {
			var oNotes = document.getElementById("notesTextarea");
			// console.log("==>>"+oNotes);
		}
	};

	// ============== INVENTORY FUNCTIONS ==============
	$scope.submitInventoryItem = function (claimId){
		var oInventoryItem = JSON.parse(window.localStorage['inventoryItems']);
		if (oInventoryItem) {
			for (var i = 0; i < oInventoryItem.length; i++) {
				delete oInventoryItem[i]._id;
				oInventoryItem[i].claimid = claimId;
				$scope.apiCall('POST', '/claimedinventory', oInventoryItem[i], true);
			};
		}
	};

	// ============== DISASTER FUNCTIONS ==============
	$scope.submitDisasterType = function (claimId){
		var dtype = window.localStorage['disasterType'];
		var oDisasterType = {
			claimid : claimId,
			type : dtype
		};
		$scope.apiCall('POST', '/disastertype', oDisasterType, true);
	};

	// ============== AREAS FUNCTIONS ==============
	$scope.submitAreas = function (claimId){
		var AffectedAreas = JSON.parse(window.localStorage['affectedAreas']);

		var oAffectedAreas = {
			claimid : claimId,
			areas : "area"
		};

		for (var i = 0; i < AffectedAreas.length; i++) {
			oAffectedAreas.areas = AffectedAreas[i];
			$scope.apiCall('POST', '/areas', oAffectedAreas, true);
		};
	};

	// ============== ROOMS FUNCTIONS ==============
	$scope.submitRooms = function (claimId){
		var AffectedRooms = JSON.parse(window.localStorage['affectedRooms']);

		var oAffectedRooms = {
			claimid : claimId,
			rooms : "room"
		};

		for (var i = 0; i < AffectedRooms.length; i++) {
			oAffectedRooms.rooms = AffectedRooms[i];
			$scope.apiCall('POST', '/rooms', oAffectedRooms, true);
		};
	};

	// ============== GEODATA FUNCTIONS ==============
	$scope.submitGeoDetails = function (claimId){
		var oGeodata = JSON.parse(window.localStorage['geodetails']);
		var sWeather = window.localStorage['weatherdetails'];
		oGeodata.claimid = claimId;
		oGeodata.weather = sWeather;
		if (oGeodata) {
			$scope.apiCall('POST', '/geodetails', oGeodata, true);
		}
	};

	// ============== MEDIA FILES FUNCTIONS ==============
	$scope.submitMediaFiles = function (claimId, data, type){

		var CONFIG = {
			application_id: 20488,
			auth_key: 'uv8BZKAkhmg7966',
			authSecret: 'NzDqwEvHeXRpNCt',
			debug: false
		};

		var config = {
			endpoints: {
				api: "api.quickblox.com"
			},
			ssl: false,
			debug: true
		};

		// initalise the environmenet with my application id, authentication key and authentication secret
		QB.init(CONFIG.application_id , CONFIG.auth_key, CONFIG.authSecret, CONFIG.debug);

		// create an API session (user is authenticated)
		var params = {email: 'clientdeskhq@gmail.com', password: '@CD2015!0'};
		//var params = {provider: 'facebook', keys: {token: 'AM46dxjhisdffgry26282352fdusdfusdfgsdf'}};

		QB.createSession(params, function(err, result) { 
			if(err) {
				console.log(err);
			}
			else {
				// console.log(result);

				if (type == 'image'){

					var imageNew = data;
					var submitedImages = [];
					var counterImage = 0;
					var x = 0;

					function submitQBImages(){
						if (imageNew[counterImage]){

							var byteCharacters = atob(imageNew[counterImage]);
							var byteNumbers = new Array(byteCharacters.length);

							for (var i = 0; i < byteCharacters.length; i++) {
								byteNumbers[i] = byteCharacters.charCodeAt(i);
							}
							
							var byteArray = new Uint8Array(byteNumbers);
							var contentType = "image/jpeg";
							var imageBlob = new Blob([byteArray], {type: contentType});

							QB.content.createAndUpload({ file: imageBlob,  name: "Image", type: "image/jpeg", public: true}, function(err, response){
								if(err) {
									console.log(err);
								}
								else {
									console.log(response);
									console.log("http://qbprod.s3.amazonaws.com/" + response.uid);
									//submit quickblox data to API
									var oMedia = {
										claimid: claimId,
										userid: "54e41a749b0f1e0e02b1b63b",
										name: "image",
										filetype: "image/jpeg",
										url: "http://qbprod.s3.amazonaws.com/" + response.uid,
										quickbloxid: response.quickbloxid,
										quickbloxuid: response.quickbloxuid
									}

									submitedImages.push(oMedia);
									counterImage ++;
									submitQBImages();
								}
							}); // done createAndUpload
						}
						else{
							if (submitedImages[x]){
								$scope.apiCall('POST', '/media', submitedImages[x], true);
								x++;
								submitQBImages();
							}
							$scope.getAudios();
						}
					} // done function submitQBImages

					submitQBImages();
				} // done if image

				if (type == 'audio'){
					
					var audioNew = data;
					var submitedAudios = [];
					var counterAudio = 0;
					var y = 0;

					function submitQBAudios(){
						if (audioNew[counterAudio]){

							audioNew[counterAudio] = JSON.parse(audioNew[counterAudio]);

							var byteCharacters = btoa(audioNew[counterAudio]);
							var byteNumbers = new Array(byteCharacters.length);

							for (var j = 0; j < byteCharacters.length; j++) {
								byteNumbers[j] = byteCharacters.charCodeAt(j);
							}
							
							var byteArray = new Uint8Array(byteNumbers);
							var contentType = "audio/wav";
							var audioBlob = new Blob([byteArray], {type: contentType});

							QB.content.createAndUpload({ file: audioBlob,  name: "Audio", type: "audio/wav", public: true}, function(err, response){
								if(err) {
									console.log(err);
								}
								else {
									console.log(response);
									console.log("http://qbprod.s3.amazonaws.com/" + response.uid);
									//submit quickblox data to API
									var oMedia = {
										claimid: claimId,
										userid: "54e41a749b0f1e0e02b1b63b",
										name: "audio",
										filetype: "audio/wav",
										url: "http://qbprod.s3.amazonaws.com/" + response.uid,
										quickbloxid: response.quickbloxid,
										quickbloxuid: response.quickbloxuid
									}

									submitedAudios.push(oMedia);
									counterAudio ++;
									submitQBAudios();
								}
							}); // done createAndUpload
						}
						else{
							if (submitedAudios[y]){
								$scope.apiCall('POST', '/media', submitedAudios[y], true);
								y++;
								submitQBAudios();
							}
							// $scope.getVideos();
						}
					} // done function submitQBAudios

					submitQBAudios();
				} // done if audio

				if (type == 'video'){

					var videoNew = data;
					var submitedVideos = [];
					var counterVideo = 0;
					var z = 0;

					function submitQBVideos(){
						if (videoNew[counterVideo]){
							// convert the video into a Blob
							var byteCharacters = atob(videoNew[counterVideo]);
							var byteNumbers = new Array(byteCharacters.length);

							for (var k = 0; k < byteCharacters.length; k++) {
								byteNumbers[k] = byteCharacters.charCodeAt(k);
							}
							
							var byteArray = new Uint8Array(byteNumbers);
							var contentType = "audio/x-wav";
							var videoBlob = new Blob([byteArray], {type: contentType});

							QB.content.createAndUpload({ file: videoBlob,  name: "Video", type: "video/mp4", public: true}, function(err, response){
								if(err) {
									console.log(err);
								}
								else {
									console.log(response);
									console.log("http://qbprod.s3.amazonaws.com/" + response.uid);
									//submit quickblox data to API
									var oMedia = {
										claimid: claimId,
										userid: "54e41a749b0f1e0e02b1b63b",
										name: "video",
										filetype: "video/mp4",
										url: "http://qbprod.s3.amazonaws.com/" + response.uid,
										quickbloxid: response.quickbloxid,
										quickbloxuid: response.quickbloxuid
									}

									submitedVideos.push(oMedia);
									counterVideo ++;
									submitQBVideos();
								}
							}); // done createAndUpload
						}
						else{
							if (submitedVideos[z]){
								$scope.apiCall('POST', '/media', submitedVideos[z], true);
								z++;
								submitQBVideos();
							}
						}
					} // done function submitQBVideos

					submitQBVideos();
				} // done if video
			} // done else
		}); // end of quickblox code
	};

	// ========================= SQLite Data =========================== //
	var imagesArray = [];
	var audiosArray = [];
	var videosArray = [];
	var sClaimID;

	$scope.getImages = function(){
		var query = "SELECT * FROM claimedImages";
		$cordovaSQLite.execute(db, query).then(function(res) {
			if(res.rows.length > 0) {
				for (var i = 0; i < res.rows.length; i++) {
					imagesArray.push(res.rows.item(i).imageData);
				}
			}
			$scope.submitMediaFiles(sClaimID, imagesArray, 'image');
		}, function (err) {
			console.error(err);
		});
	}

	$scope.getAudios = function(){
		var query = "SELECT * FROM claimedAudios";
		$cordovaSQLite.execute(db, query).then(function(res) {
			if(res.rows.length > 0) {
				for (var i = 0; i < res.rows.length; i++) {
					audiosArray.push(res.rows.item(i).audioData);
				}
			}
			$scope.submitMediaFiles(sClaimID, audiosArray, 'audio');
		}, function (err) {
			console.error(err);
		});
	}

	// $scope.getVideos = function(){
	// 	var query = "SELECT * FROM claimedVideos";
	// 	$cordovaSQLite.execute(db, query).then(function(res) {
	// 		if(res.rows.length > 0) {
	// 			for (var i = 0; i < res.rows.length; i++) {
	// 				videosArray.push(res.rows.item(i).videoData);
	// 			}
	// 		}
	// 		$scope.submitMediaFiles(sClaimID, videosArray, 'video');
	// 	}, function (err) {
	// 		console.error(err);
	// 	});
	// }

	// ============== CLAIM SUBMITION ==============
	$scope.submitClaim = function () {

		navigator.notification.confirm("How would you like to handle your claim process?", function(buttonIndex) {
			switch(buttonIndex) {
				case 1:
					console.log("Save & Send later");
					break;
				case 2:
					// console.log("Submit Now - Before");
					$scope.submitClaimItems();
					// console.log("Submit Now - After");
					break;
			}
		}, "Claim Submission", [ "Save & Send later", "Submit Now" ]);
	};

	$scope.submitClaimItems = function () {

		var oProfile;
		var sOwnership;
		
		if (window.localStorage['profile'])
			var oProfile = JSON.parse(window.localStorage['profile']);

		if (!oProfile) {
			oProfile = {
				policy : JSON.parse(window.localStorage['selectedPolicy']).policynumber,
				policyholder : JSON.parse(window.localStorage['loggedUser']).firstname + " " + JSON.parse(window.localStorage['loggedUser']).lastname,
				company : JSON.parse(window.localStorage['selectedPolicy']).company
			};
		}

		var sClaimType = window.localStorage['claimType'] ? window.localStorage['claimType'] : " ";
		var sClaimCategory = window.localStorage['claimCategory'] ? window.localStorage['claimCategory'] : " ";
		var sClaimNotes = window.localStorage['claimNotes'] ? window.localStorage['claimNotes'] : " ";     
		var dTimestamp = window.localStorage['timestamp'] ? window.localStorage['timestamp'] : " ";

		if (window.localStorage['typeOfOwnership'])
			sOwnership = JSON.parse(window.localStorage['typeOfOwnership']).text;

		var oClaim = {
			policyholder: oProfile.policyholder,
			company: oProfile.company,
			policy: oProfile.policy,
			incidenttype: sClaimCategory,
			ownership: sOwnership,
			claimtype: sClaimType,
			created: dTimestamp,
			notes: sClaimNotes
		};

		var response = $scope.apiCall('POST', '/claim', oClaim, false);

		if (response) {
			var oClaim = JSON.parse(response);
			sClaimID = oClaim._id;

			// retrieve data from SQLite
			$scope.getImages();

			if (window.localStorage['policeDetails']){
				$scope.submitPoliceDetails(sClaimID);
			}
			
			$scope.submitGeoDetails(sClaimID);
			$scope.submitInventoryItem(sClaimID);

			if (window.localStorage['claimType'] == "auto"){

				if (window.localStorage['driverDetails']){
					$scope.submitDriverDetails(sClaimID);
				}
				if (window.localStorage['vehicleDetails']){
					$scope.submitVehicleDetails(sClaimID);
				}
				if (window.localStorage['passengerDetails']){
					$scope.submitPassengerDetails(sClaimID);
				}
				if (window.localStorage['witnessDetails']){
					$scope.submitWitnessDetails(sClaimID);
				}
			}

			if (window.localStorage['claimType'] == "property"){
				
				if (window.localStorage['affectedAreas']){
					$scope.submitAreas(sClaimID);
				}
				if (window.localStorage['affectedRooms']){
					$scope.submitRooms(sClaimID);
				}
				if (window.localStorage['disasterType']){
					$scope.submitDisasterType(sClaimID);
				}
			}

			ActivityIndicator.show("Uploading Claim...");

			var delay = 6000;//1 seconds
			setTimeout(function() {
				//your code to be executed after 1 seconds
				$scope.displayAlert();
			},delay); 
			//$scope.displayAlert();
		}
	};

	$scope.displayAlert = function() {
		navigator.notification.alert("Claim Submitted Successfully", $scope.alertDismissed(), "Thank You", "OK");
	};

	$scope.alertDismissed = function() {
		$ionicHistory.nextViewOptions({
			disableAnimate: false,
			disableBack: true
		});
		$ionicHistory.clearHistory();
		$state.go('app.dashboard', {});
		ActivityIndicator.hide();
	};

	$scope.alertAutoDismissed = function() {
		ActivityIndicator.hide();
	};

	$scope.summaryScreen = function(){

		// Claim items
		var vClaimType;
		var vClaimCategory;
		var vPoliceDetails;
		var vGeoDetails;
		var vWeatherDetails;
		var vImageFiles;
		var vAudioFiles;
		var vVideoFiles;
		var vInventoryDetails;
		var vNotesDetails;
		var vPolicyDetails;

		// Auto claim items
		var vOwnership;
		var vDriverDetails;
		var vVehicleDetails;
		var vPassengerDetails;
		var vWitnessDetails;

		// Property Claim items
		var vDisasterType;
		var vAffectedAreas;
		var vAffectedRooms;

		if (window.localStorage['selectedPolicy'])
			vPolicyDetails = JSON.parse(window.localStorage['selectedPolicy']);

		if (window.localStorage['claimType'])
			vClaimType = window.localStorage['claimType'];

		if (window.localStorage['claimCategory'])
			vClaimCategory = window.localStorage['claimCategory'];

		if (window.localStorage['geodetails'])
			vGeoDetails = JSON.parse(window.localStorage['geodetails']);
		
		if (window.localStorage['weatherdetails'])
			vWeatherDetails = window.localStorage['weatherdetails'];

		if (window.localStorage['inventoryItems'])
			vInventoryDetails = JSON.parse(window.localStorage['inventoryItems']);

		if (window.localStorage['policeDetails'])
			vPoliceDetails = JSON.parse(window.localStorage['policeDetails']);

		if (window.localStorage['claimNotes'])
			vNotesDetails = window.localStorage['claimNotes'];

		var query1 = "SELECT * FROM claimedImages";
		$cordovaSQLite.execute(db, query1).then(function(res) {

			var aImages = [];

			if(res.rows.length > 0) {
				for (var i = 0; i < res.rows.length; i++) {
					aImages.push(res.rows.item(i).imageData);
				};

				vImageFiles = aImages;
			}

			var query2 = "SELECT * FROM claimedAudios";
			$cordovaSQLite.execute(db, query2).then(function(res) {

				var aAudios = [];

				if(res.rows.length > 0) {
					for (var i = 0; i < res.rows.length; i++) {
						aAudios.push(res.rows.item(i).audioData);
					};

					vAudioFiles = aAudios;
				}

				var query3 = "SELECT * FROM claimedVideos";
				$cordovaSQLite.execute(db, query3).then(function(res) {

					var aVideos = [];

					if(res.rows.length > 0) {
						for (var i = 0; i < res.rows.length; i++) {
							aVideos.push(res.rows.item(i).videoData);
						};

						vVideoFiles = aVideos;
					}

					// Auto objClaim
					if (vClaimType == "auto"){
						if (window.localStorage['driverDetails'])
							vDriverDetails = JSON.parse(window.localStorage['driverDetails']);
						
						if (window.localStorage['vehicleDetails'])				
							vVehicleDetails = JSON.parse(window.localStorage['vehicleDetails']);
						
						if (window.localStorage['passengerDetails'])
							vPassengerDetails = JSON.parse(window.localStorage['passengerDetails']);
						
						if (window.localStorage['witnessDetails'])
							vWitnessDetails = JSON.parse(window.localStorage['witnessDetails']);

						if (window.localStorage['typeOfOwnership'])
							vOwnership = JSON.parse(window.localStorage['typeOfOwnership']).text;

						$scope.objClaim = {
							claimType: vClaimType,
							claimCategory: vClaimCategory,
							ownership: vOwnership,
							driverDetails: vDriverDetails,
							vehicleDetails: vVehicleDetails,
							passengerDetails: vPassengerDetails,
							witnessDetails: vWitnessDetails,
							policeDetails: vPoliceDetails,
							geoDetails: vGeoDetails,
							weatherDetails: vWeatherDetails,
							notesDetails: vNotesDetails,
							ImageFiles: vImageFiles,
							AudioFiles: vAudioFiles,
							VideoFiles: vVideoFiles,
							inventoryDetails: vInventoryDetails,
							policyDetails: vPolicyDetails
						};
					}

					// Property objClaim
					if (vClaimType == "property"){
						if (window.localStorage['disasterType'])
							vDisasterType = window.localStorage['disasterType'];

						if (window.localStorage['affectedAreas'])
							vAffectedAreas = JSON.parse(window.localStorage['affectedAreas']);

						if (window.localStorage['affectedRooms'])
							vAffectedRooms = JSON.parse(window.localStorage['affectedRooms']);

						$scope.objClaim = {
							claimType: vClaimType,
							claimCategory: vClaimCategory,
							geoDetails: vGeoDetails,
							weatherDetails: vWeatherDetails,
							notesDetails: vNotesDetails,
							policeDetails: vPoliceDetails,
							ImageFiles: vImageFiles,
							AudioFiles: vAudioFiles,
							VideoFiles: vVideoFiles,
							inventoryDetails: vInventoryDetails,
							disasterType: vDisasterType,
							affectedAreas: vAffectedAreas,
							affectedRooms: vAffectedRooms,
							policyDetails: vPolicyDetails
						};
					}
					
				}, function (err) {
					console.error(err);
				});
				
			}, function (err) {
				console.error(err);
			});
		}, function (err) {
			console.error(err);
		});
	};

	$scope.goBack = function () {
		$ionicHistory.goBack();
	};

	$scope.goNext = function () {

		$scope.saveAditionalDetails();
		
		if (window.localStorage['claimType'] == "auto"){

			// if (!window.localStorage['driverDetails']){
			// 	navigator.notification.alert("Please, complete driver's information", $scope.alertAutoDismissed(), "Driver Details", "OK");
			// }
			// else if (!window.localStorage['vehicleDetails']){
			// 	navigator.notification.alert("Please, complete vehicle's information", $scope.alertAutoDismissed(), "Vehicle Details", "OK");
			// }
			// else if (!window.localStorage['geodetails']){
			// 	navigator.notification.alert("Please, complete geo information", $scope.alertAutoDismissed(), "Geo Details", "OK");
			// }
			// else if (!window.localStorage['weatherdetails']){
			// 	navigator.notification.alert("Please, complete weather information", $scope.alertAutoDismissed(), "Weather Details", "OK");
			// }
			// else{
			// 	location.href='#/app/mediacapture';
			// }

			location.href='#/app/mediacapture';
		}
		if (window.localStorage['claimType'] == "property"){
			$state.go('app.attachinventoryitems', {});
		}
	};

	/* ***************************************** */
	/*                 API Calls                 */
	/* ***************************************** */
	$scope.apiCall = function (methodType, resource, data, isAsync) { 
		var apiURL = 'http://127.0.0.1:3000/api';
		// var apiURL = 'http://192.168.10.104:3000/api';
		// var apiURL = 'http://dashboard.clientdesk.co:3000/api';
		var apiResource = resource;
		var apiURI  = apiURL + apiResource;

		var credentials = {
			id: 'core',
			key: 'password',
			algorithm: 'sha256'
		}

		var requestOptions = {
			uri: apiURI,
			method: methodType,
			headers: {}
		};

		var header = hawk.client.header(apiURI, methodType, { credentials: credentials, ext: 'some-app-data' });
		
		requestOptions.headers.Authorization = header.field;

		var responseJSON = null;

		$.ajaxSetup({
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
				xhr.setRequestHeader('Authorization', requestOptions.headers.Authorization);
			}
		});

		// Request with custom header
		$.ajax({
			async: isAsync,
			type: methodType,
			url: apiURI,
			success: function(response){
				responseJSON = JSON.stringify(response);
				
				if(resource == '/policies/' + userid){
					var aPolicyArray = [];
					var pPolicyArray = [];
					
					for (var i = 0; i < response.length; i++) {
						if (response[i].status == "Active") {
							if (response[i].type == "Automotive") {
								aPolicyArray.push(response[i]);
							}
							if (response[i].type == "Property") {
								pPolicyArray.push(response[i]);
							}
						}
					};

					$scope.aPolicies = aPolicyArray;
					$scope.pPolicies = pPolicyArray;
					$scope.$apply();
				}
			},
			error: function(response) {
				// alert(JSON.stringify(response));
				navigator.notification.alert("Sorry, an error has occurred", $scope.alertAutoDismissed(), "Error", "OK");
			},
			data: JSON.stringify(data),
			dataType: 'json'
		});
		return responseJSON;
	};
});