angular.module('clientdesk.services', ['firebase'])

.factory("Auth", ["$firebaseAuth", "$rootScope", function ($firebaseAuth, $rootScope) {
	var firebaseUrl = "https://clientdesk.firebaseio.com";
	var ref = new Firebase(firebaseUrl);
	return $firebaseAuth(ref);
}])

.factory('Chats', function ($firebase) {
	var selectedSession;
	var firebaseUrl = "https://clientdesk.firebaseio.com";
	var ref = new Firebase(firebaseUrl);
	var chats;

	return {
		all: function () {
			return chats;
		},
		get: function (chatId) {
			for (var i = 0; i < chats.length; i++) {
				if (chats[i].id === parseInt(chatId)) {
					return chats[i];
				}
			}
			return null;
		},
		getSelectedRoomName: function () {
			console.log("Session name in services getselectedroom- " + selectedSession);
				if (selectedSession)
					return selectedSession;
				else
					return null;
		},
		setChatSession:function(requestId){
			var oClient = JSON.parse(window.localStorage['loggedUser']);
			var clientName = oClient.firstname + " " + oClient.lastname;

			selectedSession = "";
			sessions = $firebase(ref.child('sessions')).$asObject();
			sessions.$loaded().then(function() {
				console.log("loaded record:", sessions.$id);
				// To iterate the key/value pairs of the object, use angular.forEach()
				angular.forEach(sessions, function(value, key) {
					console.log(key, value.clientname);
					if (value.clientname == clientName && value.status == "open" && value.requestid == requestId) {
						selectedSession = key;
					}
				});

				console.log("Selected Session: " + selectedSession.toString());
				if (selectedSession) {
					chats = $firebase(ref.child('sessions').child(selectedSession).child('chats')).$asArray();
				}
			});
		},
		send: function (from, message) {
			console.log("sending message from :" + from.displayName + " & message is " + message);
			if (from && message) {
				var chatMessage = {
					from: from.displayName,
					type: "text",
					message: message,
					createdAt: Firebase.ServerValue.TIMESTAMP
				};

				chats.$add(chatMessage).then(function (data) {
					console.log("message added");
				});
				var newRef = new Firebase("https://clientdesk.firebaseio.com/sessions/" + selectedSession + "/");
				newRef.update({ lastupdated: Firebase.ServerValue.TIMESTAMP, lastChatMessage: message});
			}
		}
	}
})

.factory('ChatHistory', function ($firebase) {
	var firebaseUrl = "https://clientdesk.firebaseio.com";
	var ref = new Firebase(firebaseUrl);
	var sessions = $firebase(ref.child('sessions')).$asObject();
	// = $firebase(ref.child('sessions')).$asArray();
	var fetchedSessions = {};
	var chats;

	return {
		all: function () {
			return fetchedSessions;
		},
		allChats: function(){
			return chats;
		},
		setSession:function(key){
			var newRef = new Firebase("https://clientdesk.firebaseio.com/sessions/" + key + "/" );
			chats= $firebase(newRef.child('chats')).$asArray();
			chats.$loaded().then(function() {
				console.log("chats" + chats);
			});
		},
		send: function (from, message) {
			console.log("sending message from :" + from.displayName + " & message is " + message);
			if (from && message) {
				var chatMessage = {
					from: from.displayName,
					type: "text",
					message: message,
					createdAt: Firebase.ServerValue.TIMESTAMP
				};

				chats.$add(chatMessage).then(function (data) {
					console.log("message added");
				});
			
				var newRef = new Firebase("https://clientdesk.firebaseio.com/sessions/" + selectedSession + "/");
				newRef.update({ lastupdated: Firebase.ServerValue.TIMESTAMP, lastChatMessage: message});
			}
		},
		fetchSessions: function(){
			var i = 0;
			sessions.$loaded().then(function() {
				console.log("loaded record:", sessions.$id);
				// To iterate the key/value pairs of the object, use angular.forEach()
				angular.forEach(sessions, function(value, key) {
					console.log(key, value.clientname);
					if (value.clientname == "Client1") {
						fetchedSessions[i] = {key: key, department:value.department, lastupdated:value.lastupdated, status: value.status};
						i++;
					}
				});
			});
		}
	}
})

.factory('Requests', function ($firebase) {
	var firebaseUrl = "https://clientdesk.firebaseio.com";

	// Might use a resource here that returns a JSON array
	var ref = new Firebase(firebaseUrl);
	var requests = $firebase(ref.child('requests')).$asArray();
	var callRequests = $firebase(ref.child('callrequests')).$asArray();
	var requestId;

	return {
		all: function () {
		return requests;
		},
		get: function (requestID) {
		return requests.$getRecord(requestId);
		},
		removeRequest:function(requestIDForCurrentRequest){
			var currentRequest = $firebase(ref.child('requests')).$asObject();
			currentRequest.$loaded().then(function() {
				// To iterate the key/value pairs of the object, use angular.forEach()
				angular.forEach(currentRequest, function(value, key) {
					if (value.requestid == requestIDForCurrentRequest) {
						var requestsRef = new Firebase("https://clientdesk.firebaseio.com/requests/" + key + "/");
						requestsRef.remove();
					}
				});
			});
		},
		makeCallRequest:function (userID, userName, department) {
			console.log("sending request from :" + userName + " & to the department " + department);
			if (userName && department && userID) {
				if (department == "My Agent") {
					var currentUser =  $firebase(ref.child('users').child('client').child(userID)).$asObject();
					currentUser.$loaded().then(function() {
						console.log(currentUser.brokerid);
						var request = {
							clientid: userID,
							clientname: userName,
							agentid: currentUser.brokerid,
							department: department,
							status: "unattended"
						};

						callRequests.$add(request).then(function (data) {
							console.log("Request Send");
						});
					}); 
				}
				else{
					var request = {
						clientid: userID,
						clientname: userName,
						department: department,
						status: "unattended"
					};

					callRequests.$add(request).then(function (data) {
						console.log("Request Send");
					});
				}
			}
		},
		sendReconnectRequest:function (userID, userName, department, sessionKey) {
			console.log("sending request from :" + userName + " & to the department " + department);
			if (userName && department && userID) {
				requestId = new Date() + userID;
				if (department == "My Agent") {
					var currentUser =  $firebase(ref.child('users').child('client').child(userID)).$asObject();
					currentUser.$loaded().then(function() {
						console.log(currentUser.brokerid);
						var request = {
							requestid: requestId,
							clientid: userID,
							clientname: userName,
							agentid: currentUser.brokerid,
							sessionKey: sessionKey,
							department: department,
							requestype: 'reconnect',
							priority: '1'
						};

						requests.$add(request).then(function (data) {
							console.log("Request Send");
						});
					}); 
				}
				else{
					var request = {
						requestid: requestId,
						clientid: userID,
						clientname: userName,
						department: department,
						sessionKey: sessionKey,
						requestype: 'reconnect',
						priority: '1'
					};
					requestId = request.requestid;
						requests.$add(request).then(function (data) {
						console.log("Request Send");
					});
				}
				return requestId.toString();
			}
		},
		sendRequest: function (userID, userName, department, type, priority) {
			console.log("sending request from :" + userName + " & to the department " + department);
			if (userName && department && userID) {
				requestId = new Date() + userID;
				if (department == "My Agent") {

					var currentUser =  $firebase(ref.child('users').child('client').child(userID)).$asObject();
					currentUser.$loaded().then(function() {
						console.log(currentUser.brokerid);
						var request = {
							requestid: requestId,
							clientid: userID,
							clientname: userName,
							agentid: currentUser.brokerid,
							department: department,
							requestype: type,
							priority: priority
						};

						requests.$add(request).then(function (data) {
							console.log("Request Send");
						});
					}); 
				}
				else{
					var request = {
						requestid: requestId,
						clientid: userID,
						clientname: userName,
						department: department,
						requestype: type,
						priority: priority
					};

					requestId = request.requestid;
					requests.$add(request).then(function (data) {
						console.log("Request Send");
					});
				}
				return requestId.toString();
			}
		}
	}
});