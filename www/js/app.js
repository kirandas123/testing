// Ionic ClientDesk App

var db = null;
angular.module('clientdesk', [
	'ionic',
	'firebase',
	'ngCordova',
	'ui.router',
	'angularMoment',
	'uiGmapgoogle-maps',
	'clientdesk.services',

	'AppController',
	'AttachInventoryController',
	'ChatController',
	'ChatHistoryController',
	'ClaimsController',
	'DashboardController',
	'HistoryController',
	'LoginController',
	'MailController',
	'MyClaimsController',
	'MyInventoryController',
	'MyInventoryAddController',
	'MyInventoryDetailsController',
	'MediacaptureController',
	'RoomsController',
	'ServicesController'
])

.run(function($ionicPlatform, $rootScope, $location, Auth, $ionicLoading, $cordovaSQLite) {

	$ionicPlatform.ready(function() {
		
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}
		
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}

		// $rootScope.$on("$stateChangeStart", function (e, to) {
		// 	if (to.data && to.data.requiresLogin){
		// 		if (!window.localStorage['loggedUser']){
		// 			e.preventDefault();
		// 			$location.path("/login");
		// 		}
		// 	}
		// });

		db =  $cordovaSQLite.openDB("clientdesk.db");
		$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS mails (id integer primary key, mailId text, type text, title text, description text, url text, scenario text, message text, showndate text)");
		$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS claimedImages (id integer primary key, imageData text)");
		$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS claimedAudios (id integer primary key, audioData text)");
		$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS claimedVideos (id integer primary key, videoData text)");
		$cordovaSQLite.execute(db, "DELETE FROM claimedImages");
		$cordovaSQLite.execute(db, "DELETE FROM claimedAudios");
		$cordovaSQLite.execute(db, "DELETE FROM claimedVideos");
		$rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
			// We can catch the error thrown when the $requireAuth promise is rejected
			// and redirect the user back to the home page
			if (error === "AUTH_REQUIRED") {
				$location.path("/login");
			}
		});
	});
})

.config(function($stateProvider, $urlRouterProvider) {

	$stateProvider
		.state('app', {
			url: "/app",
			abstract: true,
			templateUrl: "templates/menu.html",
			controller: 'AppCtrl', 
			data: {
				requiresLogin: true
			}
		})

		.state('app.dashboard', {
			url: "/dashboard",
			views: {
				'menuContent': {
					templateUrl: "templates/dashboard.html",
					controller: 'DashboardCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.startclaim', {
			cache: false,
			url: "/startclaim",
			views: {
				'menuContent': {
					templateUrl: "templates/startclaim.html",
					controller: 'ClaimsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.autoclaim', {
			url: "/autoclaim",
			views: {
				'menuContent': {
				templateUrl: "templates/autoclaim.html",
				controller: 'ClaimsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.propertyclaim', {
			url: "/propertyclaim",
			views: {
				'menuContent': {
					templateUrl: "templates/propertyclaim.html",
					controller: 'ClaimsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.claimitems', {
			url: "/claimitems",
			views: {
				'menuContent': {
					templateUrl: "templates/claimitems.html",
					controller: 'ClaimsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.selectdisaster', {
			url: "/selectdisaster",
			views: {
				'menuContent': {
					templateUrl: "templates/claimitems-disaster.html",
					controller: 'ClaimsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.affected', {
			cache: false,
			url: "/affectedrooms",
			views: {
				'menuContent': {
					templateUrl: "templates/claimitems-affected.html",
					controller: 'ClaimsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.addNotes', {
			url: "/addNotes",
			views: {
				'menuContent': {
					templateUrl: "templates/claimitems-other.html",
					controller: 'ClaimsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.geodata', {
			cache: false,
			url: "/geodata",
			views: {
				'menuContent': {
					templateUrl: "templates/claimitems-geodata.html",
					controller: 'ClaimsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.policedetails', {
			cache: false,
			url: "/policedetails",
			views: {
				'menuContent': {
					templateUrl: "templates/claimitems-policedetails.html",
					controller: 'ClaimsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.claimitems-geo', {
			cache: false,
			url: "/claimitems-geo",
			views: {
				'menuContent': {
					templateUrl: "templates/claimitems-geo.html",
					//,controller: 'GeoCtrl'
					controller: 'ClaimsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.services', {
			cache: false,
			url: "/services",
			views: {
				'menuContent': {
					templateUrl: "templates/services.html",
					controller: 'ServicesCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.summary', {
			cache: false,
			url: "/summary",
			views: {
				'menuContent': {
					templateUrl: "templates/summary.html",
					controller: 'ClaimsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.mediacapture', {
			cache: false,
			url: "/mediacapture",
			views: {
				'menuContent': {
					templateUrl: "templates/mediacapture.html",
					controller: 'MediacaptureCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.profile', {
			cache: false,
			url: "/profile",
			views: {
				'menuContent': {
					templateUrl: "templates/profile.html",
					controller: 'DashboardCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.myclaims', {
			cache: false,
			url: "/myclaims",
			views: {
				'menuContent': {
					templateUrl: "templates/myclaims-list.html",
					controller: 'MyClaimsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.claimdetails', {
			url: "/claimdetails/:claimid",
			cache: false,
			views: {
				'menuContent': {
					templateUrl: "templates/myclaims-details.html",
					controller: 'MyClaimsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.myinventorypolicy', {
			cache: false,
			url: "/myinventorypolicy",
			views: {
				'menuContent': {
					templateUrl: "templates/myinventory-policy.html",
					controller: 'MyInventoryCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.myinventoryitems', {
			cache: false,
			url: "/myinventoryitems",
			views: {
				'menuContent': {
					templateUrl: "templates/myinventory-items.html",
					controller: 'MyInventoryCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.myinventoryadd', {
			cache: false,
			url: "/myinventory/:inventoryCategory",
			views: {
				'menuContent': {
					templateUrl: "templates/myinventory-add.html",
					controller: 'MyInventoryAddCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.myinventorydetails', {
			cache: false,
			url: "/myinventorydetails/:inventoryId",
			views: {
				'menuContent': {
					templateUrl: "templates/myinventory-details.html",
					controller: 'MyInventoryDetailsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.attachinventoryitems', {
			cache: false,
			url: "/attachinventoryitems/",
			views: {
				'menuContent': {
					templateUrl: "templates/attach-inventory.html",
					controller: 'AttachInventoryCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.findservices', {
			cache: false,
			url: "/findservices",
			views: {
				'menuContent': {
					templateUrl: "templates/findservices.html",
					controller: 'ServicesCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.checklist', {
			cache: false,
			url: "/checklist",
			views: {
				'menuContent': {
					templateUrl: "templates/checklist.html",
					controller: 'DashboardCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('app.checklist-howto-flattire', {
			url: "/checklist-howto-flattire",
			views: {
				'menuContent': {
					templateUrl: "templates/checklist-howto-flattire.html",
					controller: 'DashboardCtrl'    
				}
			},
			data: {
				requiresLogin: true
			}
		}) 

		.state('app.gethelp', {
			url: "/gethelp",
			views: {
				'menuContent': {
					templateUrl: "templates/gethelp.html",
					controller: 'DashboardCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		}) 


		.state('app.single', {
			url: "/mails/:mailId",
				views: {
				'menuContent': {
					templateUrl: "templates/mail.html",
					controller: 'MailCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('tab', {
			url: "/tab",
			abstract: true,
			templateUrl: "templates/tabs.html",
			controller: 'RoomsCtrl',
			data:{
				requiresLogin: true
			}
		})

		.state('tab.rooms', {
			url: '/rooms',
			views: {
				'tab-rooms': {
					templateUrl: 'templates/tab-rooms.html',
					controller: 'RoomsCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('tab.history', {
			cache: false,
			url: '/history',
			views: {
				'tab-chat-history': {
					templateUrl: 'templates/tab-chat-history.html',
					controller: 'HistoryCtrl'
				}
			},
			data: {
				requiresLogin: true
			}
		})

		.state('chat', {
			url: "/chat/:requestID/:department/:clientname/:clientid/",
			templateUrl: "templates/chats.html",
			controller: 'ChatCtrl',
			data: {
				requiresLogin: true
			}
		})

		.state('chat-history', {
			cache: false,
			url: "/chat-history/:sessionKey/:sessionStatus/:department",
			templateUrl: "templates/chat-history.html",
			controller: 'ChatHistoryCtrl',
			data:{
				requiresLogin: true
			}
		})

		.state('app.login', {
			cache: false,
			url: "/login",
			views: {
				'menuContent': {
					templateUrl: "templates/login.html",
					controller: 'LoginCtrl'
				}
			}
		});

	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise('/app/login');
})

.config(function(uiGmapGoogleMapApiProvider) {
	uiGmapGoogleMapApiProvider.configure({
		key: 'AIzaSyBLHAaj5Ky_DW-NHR_1-sskg6nhwYfSlz8',
		v: '3.17',
		libraries: 'weather,geometry,visualization'
	});
})

.config(function($ionicConfigProvider) {
	$ionicConfigProvider.tabs.position("bottom"); //Places them at the bottom for all OS
	$ionicConfigProvider.tabs.style("standard"); //Makes them all look the same across all OS
});