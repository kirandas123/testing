angular.module('MyClaimsController', ['ionic', 'ui.router'])

.controller('MyClaimsCtrl', function($scope, $ionicModal, $ionicPopup, $cordovaGeolocation, $http, $state, $stateParams, $ionicHistory, $cordovaInAppBrowser, $ionicLoading, $cordovaSQLite) {

	$scope.getClaimsList = function() {
		$scope.apiCallForClaims('GET', '/claim', {}, true, 'all');
	};

	$scope.getClaimDetails = function() {

		var id = $state.params.claimid;

		// Claim details
		$scope.apiCallForClaims('GET', '/claim/' + id, {}, true, 'one');
		
		// General claims items
		$scope.apiCallForClaims('GET', '/geodetails/' + id, {}, true, 'geodetails');
		$scope.apiCallForClaims('GET', '/media/' + id, {}, true, 'media');
		$scope.apiCallForClaims('GET', '/police/' + id, {}, true, 'police');
		$scope.apiCallForClaims('GET', '/claimedinventory/claimid/' + id, {}, true, 'items');

		// Property claims items
		// $scope.apiCallForClaims('GET', '/areas/' + id, {}, true, 'areas');
		// $scope.apiCallForClaims('GET', '/rooms/' + id, {}, true, 'rooms');
		// $scope.apiCallForClaims('GET', '/disastertype/' + id, {}, true, 'disaster');

		// Auto claims items
		$scope.apiCallForClaims('GET', '/driver/' + id, {}, true, 'driver');
		$scope.apiCallForClaims('GET', '/passenger/' + id, {}, true, 'passenger');
		$scope.apiCallForClaims('GET', '/vehicle/' + id, {}, true, 'vehicle');
		$scope.apiCallForClaims('GET', '/witness/' + id, {}, true, 'witness');
	};

	$scope.goNext = function(claimID) {
		$state.go('app.claimdetails', {claimid: claimID});
	};

	$scope.apiCallForClaims = function(methodType, resource, data, isAsync, query) { 
		
		var apiURL = 'http://127.0.0.1:3000/api';
		// var apiURL = 'http://192.168.10.104:3000/api';
		// var apiURL = 'http://dashboard.clientdesk.co:3000/api';
		
		var apiResource = resource;
		var apiURI  = apiURL + apiResource;

		var credentials = {
			id: 'core',
			key: 'password',
			algorithm: 'sha256'
		}

		var requestOptions = {
			uri: apiURI,
			method: methodType,
			headers: {}
		};

		var header = hawk.client.header(apiURI, methodType, { credentials: credentials, ext: 'some-app-data' });
		
		requestOptions.headers.Authorization = header.field;

		$.ajaxSetup({
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
				xhr.setRequestHeader('Authorization', requestOptions.headers.Authorization);
			}
		});

		$scope.autoClaims = [];
		$scope.propertyClaims = [];

		// Request with custom header
		$.ajax({
			async: isAsync,
			type: methodType,
			url: apiURI,
			success: function(response){

				// Get all claims
				if (query == "all") {
					for (i = 0; i < response.length; i++) {
						response[i].created = new Date(response[i].created);
						
						if (response[i].claimtype == "auto") {
							$scope.autoClaims.push(response[i]);	
						}
						
						if (response[i].claimtype == "property") {
							$scope.propertyClaims.push(response[i]);
						}
					}
				}

				// Get specific claim
				if (query == "one"){
					response.created = new Date(response.created);
					$scope.claim = response;
				}
				
				// General Claim items
				if (query == "geodetails") {
					$scope.geodetails = response;
				}
				if (query == "police") {
					$scope.police = response;
				}
				if (query == "media") {
					$scope.media = response;
				}
				if (query == "items") {
					$scope.items = response;
				}

				// Auto Claim items
				if (query == "driver") {
					$scope.driver = response;
				}
				if (query == "passenger") {
					$scope.passenger = response;
				}
				if (query == "vehicle"){
					$scope.vehicle = response;
				}
				if (query == "witness") {
					$scope.witness = response;
				}
			},
			error: function(response) {
				console.log(JSON.stringify(response));
			},
			data: JSON.stringify(data),
			dataType: 'json'
		});
	};
});