angular.module('MailController', ['ionic','ui.router'])

.controller('MailCtrl', function($scope, $stateParams, $cordovaSQLite, $cordovaInAppBrowser, $ionicPopup) {

	var mails = [];

	function retrieveMails() {
		var query = "SELECT * FROM mails";
		$cordovaSQLite.execute(db, query).then(function(res) {
			if(res.rows.length > 0) {
				for (var i = 0; i < res.rows.length; i++) {
					console.log("Shown AT " + res.rows.item(i).showndate);
					
					if(res.rows.item(i).type == "info"){
						mails.push({id: res.rows.item(i).id, type: "info",title: res.rows.item(i).title, description: res.rows.item(i).description, url: "", showndate: res.rows.item(i).showndate});

					}
					else if(res.rows.item(i).type == "action"){
						mails.push({id: res.rows.item(i).id, type: "action", title: res.rows.item(i).title, description: res.rows.item(i).description, url: res.rows.item(i).url, showndate: res.rows.item(i).showndate});

					}
					else{
						mails.push({id: res.rows.item(i).id, type: "question", title: res.rows.item(i).title, description: res.rows.item(i).description, url: res.rows.item(i).url, message: res.rows.item(i).message, scenario: res.rows.item(i).scenario, showndate: res.rows.item(i).showndate});
					}
				}
				$scope.mails = mails;
			} else {
				$scope.mails = mails;
			}
		}, function (err) {
			console.error(err);
		});
	};

	retrieveMails();

	var options = {
		location: 'yes',
		clearcache: 'yes',
		toolbar: 'yes',
		toolbarposition: 'top',
		closebuttoncaption: 'Back to App'
	};

	$scope.$on('$cordovaInAppBrowser:exit', function(e, event){
	});

	function showMailPopUp(mail){

		if(mail.type == "action"){

			var confirmPopup = $ionicPopup.confirm({
				title: mail.title,
				template: mail.description,
				buttons: [{
					text: 'Dismiss',
					type: 'button-default',
					onTap: function(e) {
						console.log("Dismiss");
					}
				}, {
					text: 'Learn More',
					type: 'button-assertive',
					onTap: function(e) {
						console.log("The URL = " + mail.url);
						$cordovaInAppBrowser.open("http://" + mail.url, '_blank', options)
							.then(function(event) {
								// success
							})
							.catch(function(event) {
								// error
							});
					}
				}]
			});
		}
		else if(mail.type == "info"){
			//info popup
			var alertPopup = $ionicPopup.alert({
				title: mail.title,
				template: mail.description
			});
				alertPopup.then(function(res) {
			});
		}
		else{
			if(mail.scenario == "scenario2"){
				//scenario1
				var confirmPopup = $ionicPopup.confirm({
					title: mail.title,
					template: mail.description,
					buttons: [{
						text: 'Yes',
						type: 'button-default',
						onTap: function(e) {
							console.log("Yes");
							var alertPopup = $ionicPopup.alert({
								title: "Thank You",
								template: mail.message
							});
							alertPopup.then(function(res) {
							});
						}
					}, {
						text: 'No',
						type: 'button-assertive',
						onTap: function(e) {
						console.log("The URL = " + mail.url);
						$cordovaInAppBrowser.open("http://" + mail.url, '_blank', options)
							.then(function(event) {
								// success
							})
							.catch(function(event) {
								// error
							});
						}
					}]
				});
			}
			else{
				//scenario2
				var confirmPopup = $ionicPopup.confirm({
					title: mail.title,
					template: mail.description,
					buttons: [{
						text: 'Yes',
						type: 'button-assertive',
						onTap: function(e) {
							console.log("The URL = " + mail.url);
							$cordovaInAppBrowser.open("http://" + mail.url, '_blank', options)
								.then(function(event) {
									// success
								})
								.catch(function(event) {
									// error
								});
						}
					},{
						text: 'No',
						type: 'button-default',
						onTap: function(e) {
							console.log("No");
							var alertPopup = $ionicPopup.alert({
								title: "Thank You",
								template: mail.message
							});
							alertPopup.then(function(res) {
							});
						} 
					}]
				});
			}
		}
	};


	$scope.openMail = function(clickedMail){
		//alert(JSON.stringify(clickedMail));
		showMailPopUp(clickedMail);
	};

	$scope.removeMail = function(mail){
		var confirmPopup = $ionicPopup.confirm({
			title: 'Delete',
			template: 'Are you sure you want to delete the mail?'
		});
		
		confirmPopup.then(function(res) {
			if(res) {
				var query = "DELETE FROM mails WHERE id ="+mail.id;
				$cordovaSQLite.execute(db, query).then(function(res) {
					var deleteMail = document.getElementById(mail.id);
					deleteMail.parentNode.removeChild(deleteMail);
				});
			} else {
				console.log('cancel delete');
			}
		});
	};
});