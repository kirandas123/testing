angular.module('AppController', ['ionic', 'ui.router'])

.controller('AppCtrl', function($scope, $ionicPopup, $http, $state, $stateParams, $ionicHistory, $ionicLoading) {
	
	$scope.doLogout = function() {
		window.localStorage.clear();
		$state.go('app.login', {});
	};
});