angular.module('ServicesController', ['ionic','ui.router'])

.controller('ServicesCtrl', function($scope, $ionicModal, $ionicPopup, $cordovaGeolocation, $http, $state, $stateParams, $ionicHistory, $cordovaInAppBrowser, $ionicLoading, $cordovaSQLite) {

	$scope.newPosition = false;

	// var APIKey = "AIzaSyD7xslrCZwejqwy-4LRfio6E482QkAM_Rw";
	var APIKey = "AIzaSyC18OYTVOFucjFb7f53MiaXgQvhaGC4jUU";
	// var APIKey = "AIzaSyA9uhV7wrMJc4wo6SBSoprBmwdWo0XeJ8w";

	var vendorsArray, preferredVendorsArray, serviceCategory, addressLongLat, myLatitude, myLongitude;

	// Services modal
	$ionicModal.fromTemplateUrl('templates/services.html', {
		id: '1',
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true,
		backdropClickToClose: false
	}).then(function(modal) {
		$scope.oModal1 = modal;
		// $scope.$apply();
	});

	// Maps modal
	$ionicModal.fromTemplateUrl('templates/maps.html', {
		id: '2',
		scope: $scope,
		animation: 'slide-in-up',
		focusFirstInput: true,
		backdropClickToClose: false
	}).then(function(modal) {
		$scope.oModal2 = modal;
	});

	// Open + Close Modals
	$scope.openModal = function(index) {
		if(index == 1) 
			$scope.oModal1.show();
		if(index == 2) 
			$scope.oModal2.show();
	};

	$scope.closeModal = function(index) {
		if(index == 1) 
			$scope.oModal1.hide();
		if(index == 2) 
			$scope.oModal2.hide();
	};

	$scope.openServicesModal = function(category){
		serviceCategory = category;
		$scope.oModal1.show();
	};

	$scope.openMapsModal = function(navcoordinates){
		var locationCoordinates = navcoordinates.replace("(", "");
		addressLongLat = locationCoordinates.replace(")", "");
		$scope.oModal2.show();
	};

	$scope.$on('modal.shown', function(event, modal) {
		if (modal.id == 1) {

			$scope.getUserLocation();
			
			vendorsArray = [];

			function getVendorDetails(){

				if(vendors[vendorCounter]){

					var placeDetailsRequestString = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + vendors[vendorCounter].place_id + "&key=" + APIKey;

					$http.get(placeDetailsRequestString).
					success(function(data, status, headers, config) {

						var vendorDetailResult = data.result;
						var openNow;
						var openHours = [];
						var vendorLatitude, vendorLongitude;

						if(vendorDetailResult.geometry){
							if(vendorDetailResult.geometry.location){
								vendorLatitude = vendorDetailResult.geometry.location.lat;
								vendorLongitude = vendorDetailResult.geometry.location.lng;
							}
						}
						if(vendorDetailResult.opening_hours){
							if (vendorDetailResult.opening_hours.weekday_text) {
								openHours = vendorDetailResult.opening_hours.weekday_text;
							}
							if(vendorDetailResult.opening_hours.open_now == true){
								openNow = "Yes";
							}
							else{
								openNow = "No";
							}
						}
						else{
							openNow = "N/A";
							openHours = [];
						}

						var photoURL;
						if(vendorDetailResult.photos){
							photoURL = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=100&photoreference=" +  vendorDetailResult.photos[0].photo_reference+ "&key=" + APIKey;
						}
						else{
							photoURL = "img/noimage.png"; 
						}

						var phone;
						if(vendorDetailResult.international_phone_number){
							phone = vendorDetailResult.international_phone_number;
						}
						else{
							phone = "";
						}

						var vendor_address;
						if(vendorDetailResult.formatted_address){
							vendor_address = vendorDetailResult.formatted_address;
						}
						else{
							vendor_address = "Address : N/A";
						}

						vendorsArray.push({
							name: vendors[vendorCounter].name,
							address: vendor_address,
							phone: phone,
							photo: photoURL,
							openHours: openHours,
							openNow: openNow,
							latitude: vendorLatitude,
							longitude: vendorLongitude
						});

						vendorCounter++;

						getVendorDetails();
					})
					.error(function(data, status, headers, config) {
					});
				}// end of if
				else{
					$scope.vendors = $scope.checkLocalDist(vendorsArray);
					// $scope.$apply();
					$ionicLoading.hide();
				}
			}// end of getvendordetails

			var requestString;
			if((serviceCategory == 'collision+center') || (serviceCategory == 'tow+truck')){
				requestString = "https://maps.googleapis.com/maps/api/place/search/json?location=" + myLatitude + "," + myLongitude + "&radius=10000" + "&name=" + serviceCategory + "&sensor=false&key=" + APIKey;
				console.log(requestString);
			}
			else{
				requestString = "https://maps.googleapis.com/maps/api/place/search/json?location=" + myLatitude + "," + myLongitude + "&radius=10000" + "&types=" + serviceCategory + "&sensor=false&key=" + APIKey;
				console.log(requestString);
			}

			$http.get(requestString)
				.success(function(data, status, headers, config) {
					vendors = data.results;
					vendorCounter = 0;
					getVendorDetails();
				})
				.error(function(data, status, headers, config) {
				});

			$scope.vendors = $scope.checkLocalDist(vendorsArray);
			$scope.apiCallForServices('GET', '/service',{},true);
		} // end of if modal.id == 1
		
		if (modal.id == 2) {
			$scope.ShowLocation(addressLongLat);
		}
	}); // end $scope.$on shown

	$scope.$on('$destroy', function() {
		$scope.oModal1.remove();
		$scope.oModal2.remove();
	});

	$scope.getUserLocation = function(){

		$scope.newPosition = false;

		var posOptions = {timeout: 5000, enableHighAccuracy: true};
		
		$cordovaGeolocation
			.getCurrentPosition(posOptions)
			.then(function (position) {
				myLatitude  = position.coords.latitude;
				myLongitude = position.coords.longitude;
				$scope.newPosition = true;
			}, function(err) {
				alert(JSON.stringify(err));
			});
	};

	$scope.ShowLocation = function(coord){

		var commaIndex = coord.indexOf(",");
		var vendorLat = coord.slice(0, commaIndex);
		var vendorLong = coord.slice(commaIndex + 1, coord.length);

		$scope.latitude = vendorLat;
		$scope.longitude = vendorLong;

		$scope.map = { center: { latitude: vendorLat, longitude: vendorLong }, zoom: 18 };

		$scope.marker = {
			id: 0,
			coords: {
				latitude: vendorLat,
				longitude: vendorLong
			},
			options: { 
				draggable: false,
				scrollwheel: true,
				zoomControl: true,
			}
		};
	};

	$scope.NavigateToLocation = function(navLatitude, navLongitude){

		launchnavigator.navigate(
			[navLatitude, navLongitude],
			[myLatitude, myLongitude],

			function(){
				// console.log("Success");
			},

			function(error){
				console.log(error);
			});
	};

	$scope.checkPreferredDist = function(VendorResponse){
		var removeTrash = VendorResponse.coordinates.replace("(", "");
		var newCoordinates = removeTrash.replace(")", "");
		var commaIndex = newCoordinates.indexOf(",");
		var vendorLat = newCoordinates.slice(0, commaIndex);
		var vendorLong = newCoordinates.slice(commaIndex + 1, newCoordinates.length);

		var from = new google.maps.LatLng(myLatitude, myLongitude);
		var to   = new google.maps.LatLng(vendorLat, vendorLong);
		var dist = google.maps.geometry.spherical.computeDistanceBetween(from, to);

		dist = parseFloat(dist)/1000;
		
		if (dist < 50){
			var VendorResponsePhoto = "img/noimage.png";
			preferredVendorsArray.push({
				name: VendorResponse.name, 
				address: VendorResponse.address, 
				phone: VendorResponse.phone, 
				photo: VendorResponsePhoto, 
				discount: VendorResponse.discount, 
				coordinates: VendorResponse.coordinates, 
				distance: dist
			});
		}
	};

	$scope.checkLocalDist = function(Vendors){

		var VendorsArray = [];

		for(i = 0; i < Vendors.length; i++){
			var from = new google.maps.LatLng(myLatitude, myLongitude);
			var to   = new google.maps.LatLng(Vendors[i].latitude, Vendors[i].longitude);
			var dist = google.maps.geometry.spherical.computeDistanceBetween(from, to);

			dist = parseFloat(dist)/1000;
			
			VendorsArray.push({
				name: Vendors[i].name,
				address: Vendors[i].vendor_address,
				phone: Vendors[i].phone,
				photo: Vendors[i].photo,
				openHours: Vendors[i].openHours,
				openNow: Vendors[i].openNow,
				latitude: Vendors[i].latitude,
				longitude: Vendors[i].longitude,
				distance: dist
			});
		}

		return VendorsArray;
	};

	/* ***************************************** */
	/*                 API Calls                 */
	/* ***************************************** */
	$scope.apiCallForServices = function (methodType, resource, data, isAsync) {
		
		var apiURL = 'http://127.0.0.1:3000/api';
		// var apiURL = 'http://192.168.10.104:3000/api';
		// var apiURL = 'http://dashboard.clientdesk.co:3000/api';
		
		var apiResource = resource;
		var apiURI  = apiURL + apiResource;

		var credentials = {
			id: 'core',
			key: 'password',
			algorithm: 'sha256'
		}

		var requestOptions = {
			uri: apiURI,
			method: methodType,
			headers: {}
		};

		var header = hawk.client.header(apiURI, methodType, { credentials: credentials, ext: 'some-app-data' });
		requestOptions.headers.Authorization = header.field;

		var responseJSON = null;
		preferredVendorsArray = [];

		$.ajaxSetup({
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
				xhr.setRequestHeader('Authorization', requestOptions.headers.Authorization);
			}
		});

		// Request with custom header
		$.ajax({
			async: isAsync,
			type: methodType,
			url: apiURI,
			success: function(response){
				for(var i =0; i<response.length; i++){
					if(serviceCategory == response[i].category){
						// var preferredVendorName = response[i].name;
						// var preferredVendorAddress = response[i].address;
						// var preferredVendorPhone = response[i].phone;
						// var preferredVendorPhoto = "img/noimage.png";
						// var preferredVendorDiscount = response[i].discount;
						// var preferredVendorCoordinates = response[i].coordinates;
						// preferredVendorsArray.push({name:preferredVendorName, address:preferredVendorAddress, phone:preferredVendorPhone, photo:preferredVendorPhoto, discount:preferredVendorDiscount, coordinates:preferredVendorCoordinates});
						$scope.checkPreferredDist(response[i]);
					}
				}
				$scope.preferredVendors = preferredVendorsArray;
				$scope.$apply();
			},
			error: function(response) {
				//responseJSON = JSON.stringify(response);
				alert(JSON.stringify(response));
			},
			data: JSON.stringify(data),
			dataType: 'json'
		}); // scheduledPrompts = responseJSON;
	};
});